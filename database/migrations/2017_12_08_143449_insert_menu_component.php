<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertMenuComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        $menuData = [];
        $menuData[] = [

            'name' => 'Home',
            'url'  => '/'

        ];
        
        $menu = new Component();
        $menu->name = "Menu";
        $menu->slug = "menu";
        $menu->data = json_encode($menuData);
        $menu->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        $menu = Component::where('slug','menu')->first();

        if($menu)
            $menu->delete();

    }
}
