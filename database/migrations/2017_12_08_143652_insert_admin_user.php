<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        $user = new User();
        $user->role = 0;
        $user->name = "Admin";
        $user->email = "admin@moringaforlife.pk";
        $user->password = bcrypt("admin123");
        $user->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){

        $user = User::where('email','admin@moringaforlife.pk')->first();

        if($user)
            $user->delete();

    }
}
