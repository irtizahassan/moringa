<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertContactComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        $contactData = [];
        $contactData['phone'] = "";
        $contactData['email'] = "";
        $contactData['address'] = "";

        $contact = new Component();
        $contact->name = "Contact Info";
        $contact->slug = "contactinfo";
        $contact->data = json_encode($contactData);
        $contact->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        $contact = Component::where('slug','contactinfo')->first();

        if($contact)
            $contact->delete();

    }
}
