<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertBusinessComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $businessData = [
            'heading' => " MoringaForLife",
            'details' => 'Moringa (Moringa oleifera Lam.) is native to the India and Pakistan and has become naturalised in the tropical and subtropical areas around the world. Almost all parts of the Moringa tree are used as food, natural food supplements and cosmetics. It is considered as one of the World’s most useful trees, as almost every part of the Moringa tree can be used for food, medication and industrial purposes. <a>Readmore</a>'
        ];
        $business = new Component();
        $business->name = "Business";
        $business->slug = "business";
        $business->data = json_encode($businessData);
        $business->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $business = Component::where('slug','business')->first();

        if($business)
            $business->delete();
    }
}
