<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertAboutMeComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){

        $aboutmeData = [

            'img' => 'http://moringa.dev/images/team_1.jpg',
            'heading'  => 'Managing Director',

            'details' => 'I am Gulrez Shahzad. I have studied M.Sc (Hons.) Agronomy. With the background in agricultural trials and research, and expertise in agribusiness development, I represent G S Agrizone Ltd across the Pakistan and Europe/UK. I oversee the range of projects across the division. Focus areas include agronomy, moringa nursery raising, moringa farming under good agricultural practices (GAP), moringa leaf extract, moringa as livestock feed, moringa biostimulants and product development. I am passionate about working on this remarkable nutritious herbal tree “Moringa oleifera" to explore its benefits to impact the climate change.'

        ];
        
        $aboutme = new Component();
        $aboutme->name = "About Me";
        $aboutme->slug = "aboutme";
        $aboutme->data = json_encode($aboutmeData);
        $aboutme->save();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        $aboutme = Component::where('slug','aboutme')->first();

        if($aboutme)
            $aboutme->delete();

    }
}
