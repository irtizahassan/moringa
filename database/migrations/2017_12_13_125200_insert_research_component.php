<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertResearchComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        $researchData = [
            'heading' => " Research Papers",
            'details' => '<p><a href="#">(2011) Moringa Leaf Extract</a><br />
                            <a href="#">(2013) Moringa Foliar Wheat</a><br />
                            <a href="#">(2013) Moringa Biomass</a><br />
                            <a href="#">(2014) Moringa Water Purification</a><br />
                            <a href="#">(2015) Moringa Biomass Spacing</a><br />
                            <a href="#">(2016) Basra &amp; Lovatt</a><br />
                            <a href="#">Moringa(Importance, use &amp; Cultivation</a>
                        </p>'
        ];
        $research = new Component();
        $research->name = "Research";
        $research->slug = "research";
        $research->data = json_encode($researchData);
        $research->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        $research = Component::where('slug','research')->first();

        if($research)
            $research->delete();

    }
}
