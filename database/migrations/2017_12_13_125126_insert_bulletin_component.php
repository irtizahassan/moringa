<?php

use App\Component;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertBulletinComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        
        $bulletinData = [];
        $bulletin = new Component();
        $bulletin->name = "Bulletin";
        $bulletin->slug = "bulletin";
        $bulletin->data = json_encode($bulletinData);
        $bulletin->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        
        $bulletin = Component::where('slug','bulletin')->first();

        if($bulletin)
            $bulletin->delete();
        
    }
}
