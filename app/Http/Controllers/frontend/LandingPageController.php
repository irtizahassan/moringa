<?php

namespace App\Http\Controllers\frontend;

use App\News;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LandingPageController extends Controller{

	public function index(){
		
		$aboutme  = Component::where('slug','aboutme')->first();
		$aboutme  = json_decode($aboutme->data,true);

		$research  = Component::where('slug','research')->first();
		$research  = json_decode($research->data,true);

		$business  = Component::where('slug','business')->first();
		$business  = json_decode($business->data,true);

		$newsfeed = News::where('posted',1)->get();

		return view('frontend.index',compact('newsfeed','aboutme','research','business'));

	}

}
