<?php

namespace App\Http\Controllers\frontend;

use Auth;
use App\News;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller{
    
	public function display($slug){

		$page = Page::where('slug',$slug)->first();

		if(!$page)
			abort(404);

		if($page->published == 0 && ( !Auth::check() || (Auth::check() && Auth::user()->role != 0)))
			abort(404);

		$title = $page->title;

		$newsfeed = News::where('posted',1)->get();

		return view('frontend.pages.display',compact('page','title','newsfeed'));

	}

	public function contactUs(Request $request){

		return view('frontend.pages.contactus');
	
	}

	public function farmersSupport(Request $request){

		return view('frontend.pages.farmers-support');

	}

    public function weather(Request $request){

        return view('frontend.pages.weather');

    }

    public function festival()
    {
        return view('frontend.pages.gallery');
    }

    public function moringa_img()
    {
        return view('frontend.pages.moringa_gallery');
    }

    public function moringa_seeds()
    {
        return view('frontend.pages.moringa_seeds');
    }


    public function feasibility()
    {
        return view('frontend.pages.feasibility');
    }

	// public function get_client_ip() {
	//     $ipaddress = '';
	//     if (isset($_SERVER['HTTP_CLIENT_IP']))
	//         $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	//     else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	//         $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	//     else if(isset($_SERVER['HTTP_X_FORWARDED']))
	//         $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	//     else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	//         $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	//     else if(isset($_SERVER['HTTP_FORWARDED']))
	//         $ipaddress = $_SERVER['HTTP_FORWARDED'];
	//     else if(isset($_SERVER['REMOTE_ADDR']))
	//         $ipaddress = $_SERVER['REMOTE_ADDR'];
	//     else
	//         $ipaddress = '';
	//     return $ipaddress;
	// }

}
