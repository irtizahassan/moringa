<?php

namespace App\Http\Controllers\backend;

use Session;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinessController extends Controller{
    
	public function index(){
		
		$business = Component::where('slug','business')->first();
		$business = json_decode($business->data,true);

		return view('backend.business.index',compact('business'));

	}

	public function save(Request $request){
		
		$heading = $request->input('heading');
		$details = $request->input('details');

		$data = [
			'heading' => $heading,
			'details' => $details,
		];

		$business = Component::where('slug','business')->first();
		$business->data = json_encode($data);
		$business->save();

		Session::flash("message","Successfully Updated");

		return redirect()->back();

	}

}
