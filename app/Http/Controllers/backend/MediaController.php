<?php

namespace App\Http\Controllers\backend;

use URL;
use Session;
use App\File;
use Exception;
use App\Folder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FileUploadRequest;

class MediaController extends Controller{
    
	public function filesView(){

		$files = File::all();

		return view('backend.media.files.index',compact('files'));
		
	}

	public function fileUpload(){

		$folders = Folder::pluck('name');

		return view('backend.media.files.upload',compact('folders'));
	
	}

	public function fileUploadProcess(FileUploadRequest $request){

		// $name 	= $request->name;
		$folder = $request->folder;

		// $name 	= str_replace(" ", "_", $name);
		// $name 	= str_replace("/", "_", $name);

		if($folder != "/")
			$folder = "/".$folder."/";

		

		$path = storage_path('app/public').$folder;
		$ext  = $request->file('file')->getClientOriginalExtension();

		$file = str_random(20).".".$ext;
//		if(File::where('name',$file)->where('folder',$folder)->first()){
//
//						Session::flash("errormessage","File with same name already exists in folder.");
//						return redirect()->back();
//
//					}

		$request->file('file')->move(
            $path, $file
        );

        $uFile = new File();
		$uFile->name 	= $file;
		$uFile->folder 	= $request->folder;
		$uFile->path 	= $folder.$file;
		$uFile->save();

		Session::flash("message","File Uploaded Successfully...");

		return redirect()->back();
        
	}

	public function folderCreate(){

		return view('backend.media.folder.create');
	
	}

	public function folderCreateProcess(Request $request){

		$request->name = str_replace(" ", "", $request->name);
		
		if(Folder::where('name',$request->name)->first()){

			Session::flash("errormessage","Folder Already Exists...");	
			
			if(!file_exists(storage_path('app/public/').$request->name)){
				mkdir(storage_path('app/public/').$request->name,0777,true);
			}		

		}else{
			
			if(!file_exists(storage_path('app/public/').$request->name)){

				if(mkdir(storage_path('app/public/').$request->name,0777,true)){

					$folder = new Folder;
					$folder->name = $request->name;
					$folder->path = "";
					$folder->save();

					Session::flash("message","Folder Created Successfully...");
					
				}else{

					Session::flash("errormessage","Folder Creation Failed...");	
				
				}

			}else{

				$folder = new Folder;
				$folder->name = $request->name;
				$folder->path = "";
				$folder->save();

				Session::flash("message","Folder Created Successfully...");

			}		

		}

		return redirect()->back();

	}

}
