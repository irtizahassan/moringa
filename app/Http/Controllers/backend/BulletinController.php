<?php

namespace App\Http\Controllers\backend;

use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BulletinController extends Controller{
    
	public function index(){

		$bulletin = Component::where('slug','bulletin')->first();

		if(!$bulletin)
			return redirect()->back();

		$bulletin = json_decode($bulletin->data);	

		return view('backend.bulletin.index',compact('bulletin'));

	}

	public function save(Request $request){


		$bulletinArray = [];
		foreach(json_decode($request->bulletin) as $heading => $url){

			$bullet = [];
			$bullet['heading'] = $heading;
			$bullet['url'] = $url;

			$bulletinArray[] = $bullet;

		}

		$bulletin = Component::where('slug','bulletin')->first();
		$bulletin->data = json_encode($bulletinArray);
		$bulletin->save();

		return [
			'status' => 'success'
		];

	}

}
