<?php

namespace App\Http\Controllers\backend;

use Auth;
use Hash;
use Session;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PasswordController extends Controller
{
    
    public function changePasswordProcess(Request $request){

    	if(Hash::check($request->old, Auth::user()->password) && ($request->new == $request->confirm)){

    		$user = User::find(Auth::user()->id);
    		$user->password = Hash::make($request->new);
    		$user->save();
    		
    		return redirect()->route('admin.dashboard');  

    	}

    	Session::flash("error","Oops Something wasn't right.");
        
        return redirect()->back();

    }

}
