<?php

namespace App\Http\Controllers\backend;

use Session;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller{
    
	public function index(){

		$news = News::all();

		return view('backend.news.index',compact('news'));

	}

	public function create(){

		return view('backend.news.create');

	}

	public function createProcess(Request $request){

		$news = new News();
		$news->content = $request->content;
		$news->time = 0;

		Session::flash("message","News Posted Successfully...");

		$pass = true;
		
		if(isset($request->time)){

			$news->posted = 0;
			$news->time = strtotime($request->time);

			if(time() > strtotime($request->time))
				$pass = false;

			Session::flash("message","News Scheduled Successfully...");

		}

		if(!$pass){

			Session::flash("errormessage","Requires Future DateTime...");
			Session::flash("message","");


		}else{

			if(!$news->save()){

				Session::flash("errormessage","Action Failed...");
				Session::flash("message","");

			}

		}

		return redirect()->back();

	}

	public function edit($id){

		$news = News::find($id);

		return view('backend.news.edit',compact('news'));

	}

	public function editProcess(Request $request){

		$news = News::find($request->id);

		if($news){

			$news->content = $request->content;

			$news->save();

		}

		return redirect()->route('admin.news.index');

	}

	public function postNow(Request $request){

		$news = News::find($request->id);

		if($news){

			$news->posted = 1;
			$news->save();

		}

		return redirect()->back();

	}

	public function delete(Request $request){

		$news = News::find($request->index);

		if($news){

			$news->delete();

		}		

		return redirect()->back();

	}

}
