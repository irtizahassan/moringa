<?php

namespace App\Http\Controllers\backend;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PagesController extends Controller {

	public function get() {

		$pages = Page::get(['id','slug','title','published','created_at','updated_at']);

		return view('backend.pages.viewall',compact('pages'));

	}

	public function create(){

		$hasEditor = true;

		return view('backend.pages.create',compact('hasEditor'));

	}

	public function createProcess(Request $request){

		$page = new Page();
		$page->title = $request->title;
		$page->slug = str_replace(" ", "_", $request->title);
		$page->content = ($request->content == null) ? "" : $request->content;

		$page->save();

		return redirect('admin/pages');

	}

	public function edit($page_id){

		$page = Page::find($page_id);

		if(!$page)
			return redirect()->back();

		$hasEditor = true;

		return view('backend.pages.edit',compact('page','hasEditor'));

	}

	public function editProcess(Request $request){

		$page = Page::find($request->id);

		if($page){

			$page->title = $request->title;
			$page->slug = $request->slug;
			$page->content = ($request->content == null) ? "" : $request->content;

			$page->save();

		}
		
		return redirect('admin/pages');

	}

	public function statusChange(Request $request){

		$page = Page::find($request->id);

		$page->published = ($page->published == 0 ? 1 : 0);
		$page->save();

		return redirect()->back();

	}

	public function destroy($page_id){

		Page::destroy($page_id);

		return redirect()->back();

	}
    
}
