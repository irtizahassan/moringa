<?php

namespace App\Http\Controllers\backend;

use Session;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutMeController extends Controller{
    
	public function index(){

		$aboutme  = Component::where('slug','aboutme')->first();
		$aboutme  = json_decode($aboutme->data,true);
		return view('backend.aboutme.index',compact('aboutme'));

	}

	public function update(Request $request){

		$aboutme  = Component::where('slug','aboutme')->first();

		if($aboutme){

			$aboutmeData = [

	            'heading'  => $request->heading,
	            'details' => $request->details

	        ];

	        $aboutme->data = json_encode($aboutmeData);
	        $aboutme->save();

		}

		Session::flash("message","Section Updated Successfully...");
		
		return redirect()->back();

	}

}
