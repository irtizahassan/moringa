<?php

namespace App\Http\Controllers\backend;

use Session;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller{
    
	public function index(){

		$menu = Component::where('slug','menu')->first();

		if($menu) 
			$menuItems = json_decode($menu->data);
		else{

			$menuComponent = new Component();
			$menuComponent->name = "Menu";
			$menuComponent->slug = "menu";
			$menuComponent->data = json_encode([]);

			$menuComponent->save();
			
			$menuItems = [];
		
		}

		return view('backend.menu.index',compact('menuItems'));

	}

	public function create(){

		return view('backend.menu.create');

	}

	public function createProcess(Request $request){

		$menuComponent = Component::where("slug","menu")->first();

		$menuList = json_decode($menuComponent->data,true);
	
		$menuList[] = [

			'name' => $request->name,
			'url'  => $request->url

		];

		$menuComponent->data = json_encode($menuList);
		$menuComponent->save();

		\Session::flash("message","Item Added To Menu.");

		return redirect()->back();

	}

	public function edit($itemIndex){

		$menuComponent = Component::where("slug","menu")->first();

		$menuItems = json_decode($menuComponent->data,true);

		$item = $menuItems[$itemIndex];
	
		return view('backend.menu.edit',compact('item','itemIndex'));

	}

	public function editProcess(Request $request){

		$menuComponent = Component::where("slug","menu")->first();

		$menuItems = json_decode($menuComponent->data,true);

		$menuItems[$request->index] = [

			'name' => $request->name,
			'url'  => $request->url

		];

		$menuComponent->data = json_encode($menuItems);
		
		if($menuComponent->save()){

			Session::flash("message","Update Successfull...");

		}else{

			Session::flash("errormessage","Update Failed...");			

		}

		return redirect()->back();

	}

	public function sort(){

		$menuComponent = Component::where("slug","menu")->first();

		$menuItems = json_decode($menuComponent->data,true);
	
		return view('backend.menu.sort',compact('menuItems'));

	}

	public function sortProcess(Request $request){

		$menuComponent = Component::where("slug","menu")->first();

		$menuComponent->data = json_encode($request->menu);
		$menuComponent->save();

		return url(route('admin.menu.index'));

	}

	public function deleteProcess(Request $request){

		$menuComponent = Component::where("slug","menu")->first();

		$menuItems = json_decode($menuComponent->data,true);

		$menu = [];

		foreach($menuItems as $key => $item){
			
			if($request->index == $key)
				continue;

			$menu[] = $item;

		}

		$menuComponent->data = json_encode($menu);
		$menuComponent->save();

		return redirect()->back();

	}

}
