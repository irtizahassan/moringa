<?php

namespace App\Http\Controllers\backend;

use Mail;
Use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactUs as ContactUsEmail;
use App\Mail\FarmersSupport as FarmerSupportEmail;

class EmailController extends Controller{
    
	public function contactUs(Request $request){

		$status = "success";

		$name = trim($request->input('first_name')." ".$request->input('last_name')); 
		$email = $request->input('email'); 
		$phone = $request->input('phone');
		$message = $request->input('message'); 

		Mail::to('moringaforlife786@gmail.com')->queue(new ContactUsEmail($name,$email,$phone,$message));

		Session::flash('status','Success');
		return redirect()->back();

	}

	public function farmerSupport(Request $request){

		$status = "success";

		$name = trim($request->input('first_name')." ".$request->input('last_name')); 
		$email = $request->input('email'); 
		$phone = $request->input('phone'); 
		$message = $request->input('message'); 

		Mail::to('moringaforlife786@gmail.com')->queue(new FarmerSupportEmail($name,$email,$phone,$message));

		Session::flash('status','Success');
		return redirect()->back();

	}

}
