<?php

namespace App\Http\Controllers\backend;

use Session;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResearchController extends Controller{
    
	public function index(){
		
		$research = Component::where('slug','research')->first();
		$research = json_decode($research->data,true);

		return view('backend.research.index',compact('research'));

	}

	public function save(Request $request){
		
		$heading = $request->input('heading');
		$details = $request->input('details');

		$data = [
			'heading' => $heading,
			'details' => $details,
		];

		$research = Component::where('slug','research')->first();
		$research->data = json_encode($data);
		$research->save();

		Session::flash("message","Successfully Updated");

		return redirect()->back();

	}

}
