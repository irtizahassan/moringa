<?php

namespace App\Http\Controllers\backend;

use Session;
use App\Component;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller{

	public function index(){

		$contactComponent = Component::where("slug","contactinfo")->first();

		if(!$contactComponent){

			$contactComponent 		= new Component();
			$contactComponent->name = "Contact Info";
			$contactComponent->slug = "contactinfo";
			$contactComponent->data = json_encode([

				'phone' 	=> '',
				'email' 	=> '',
				'email2' 	=> '',
				'address' 	=> ''

			]);

			$contactComponent->save();

		}

		$contactInfo = json_decode($contactComponent->data,true);

		if(!isset($contactInfo['email2'])){

			$contactInfo['email2'] = "";

		}

		return view('backend.contactinfo.index',compact('contactInfo'));

	}

	public function update(Request $request){

		$data = [

			'phone' 	=> $request->phone,
			'email' 	=> $request->email,
			'email2' 	=> $request->email2,
			'address' 	=> $request->address

		];

		$contactComponent = Component::where("slug","contactinfo")->first();

		$contactComponent->data = json_encode($data);

		if($contactComponent->save()){

			Session::flash("message","Update Successfull...");

		}else{

			Session::flash("errormessage","Update Failed...");

		}

		return redirect()->back();

	}

}
