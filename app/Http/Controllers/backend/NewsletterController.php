<?php

namespace App\Http\Controllers\backend;

use Mail;
use Illuminate\Http\Request;
use App\NewsletterSubscriber;
use App\Http\Controllers\Controller;
use App\Mail\NewsletterSubscriptionEmail;

class NewsletterController extends Controller{
    
	public function subscribe(Request $request){

		$errorRespone = [
			'status' => 'error'
		];

		$successResponse = [
			'status' => 'success'
		];

		$email = $request->input('email');
		$email = trim($email);

		if($email == "")
			return response()->json($errorRespone);

		$exists = NewsletterSubscriber::where('email',$email)->first();
		
		if($exists && $exists->isActive == 0){

			$exists->isActive = 1;
			
			if($exists->save())
				return response()->json($successResponse);

			return response()->json($errorRespone);

		}

		if($exists && $exists->isActive == 1){

			return response()->json($successResponse);
			
		}

		$subscriber = new NewsletterSubscriber;
		$subscriber->email 		= $email;
		$subscriber->isActive 	= 1;
		
		if($subscriber->save()){
			Mail::to($email)->queue(new NewsletterSubscriptionEmail($email));
			return response()->json($successResponse);
		}

		return response()->json($errorRespone);

	}

	public function unsubscribe($email){

		$subscriber = NewsletterSubscriber::where('email',$email)->first();

		if($subscriber && $subscriber->isActive == 1){
			$subscriber->isActive = 0;
			$subscriber->save();
			return "<script>window.close();</script>";
		}
		
		return '<div style="display:flex; align-items:center; justify-content:center; width:100vw; height:100vh; margin:-8px">
					<h2>We hate to see you leave and hope you come back soon<h2>
				</div>';
	}

	public function subscribers(){

		$subscribers = NewsletterSubscriber::where('isActive',1)->get(['email','updated_at']);

		return view('backend.newsletter.subscribers',compact('subscribers'));

	}

}
