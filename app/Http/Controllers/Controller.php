<?php

namespace App\Http\Controllers;

use View;
use App\Component;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){

    	$bulletin = Component::where('slug','bulletin')->first();
		$bulletin = json_decode($bulletin->data,true);

		View::share('bulletin',$bulletin);

    	$menuComponent = Component::where('slug','menu')->first();
		$menuItems     = json_decode($menuComponent->data,true);

    	View::share('menuItems',$menuItems);

    	$contactComponent = Component::where('slug','contactinfo')->first();
		$contactInfo      = json_decode($contactComponent->data,true);

    	View::share('contactInfo',$contactInfo);

    }
}
