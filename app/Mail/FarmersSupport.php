<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FarmersSupport extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $phone;
    public $messager;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $phone, $message){
        
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->messager = $message;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        return $this->from('info@moringaforlife.pk','MoringaForLife')
                    ->subject('Moringaforlife Farmers Support ('.date('Y-m-d h:i:s').')')
                    ->view('emails.farmers-support');
    
    }
}
