<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewsletterSubscriptionEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email){
        
        $this->email = $email;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){

        return $this->from('info@moringaforlife.pk','MoringaForLife')
                    ->subject('Moringaforlife Newsletter Subscription')
                    ->view('emails.newsletter-subscription-successfull');
    
    }
}
