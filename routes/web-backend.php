<?php

	if (env('APP_ENV') === 'production') {
	    URL::forceScheme('https');
	}

	Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {

		Route::view('/', 'backend.dashboard')->name('admin.dashboard');

		Route::group(['prefix' => 'menu'], function(){

			Route::get('/','backend\MenuController@index')->name('admin.menu.index');
			Route::get('/item/create','backend\MenuController@create')->name('admin.menu.item.create');
			Route::post('/item/create/process','backend\MenuController@createProcess')->name('admin.menu.item.create.process');
			Route::get('/item/edit/{id}','backend\MenuController@edit')->name('admin.menu.item.edit');
			Route::post('/item/edit/process','backend\MenuController@editProcess')->name('admin.menu.item.edit.process');
			Route::post('/item/delete/process','backend\MenuController@deleteProcess')->name('admin.menu.item.delete.process');
			Route::get('/sort','backend\MenuController@sort')->name('admin.menu.sort');
			Route::post('/sort/process','backend\MenuController@sortProcess')->name('admin.menu.sort.process');

		});

		Route::group(['prefix' => 'pages'], function(){

			Route::get('/', 'backend\PagesController@get')->name('viewpages');
			Route::post('/', 'backend\PagesController@get')->name('viewpages');
			Route::get('/create', 'backend\PagesController@create');
			Route::post('/create/process', 'backend\PagesController@createProcess');
			Route::get('/edit/{page_id}', 'backend\PagesController@edit')->name('admin.page.edit.process');
			Route::post('/edit/process', 'backend\PagesController@editProcess');
			Route::get('/delete/{page_id}', 'backend\PagesController@destroy');
			Route::post('/status/process', 'backend\PagesController@statusChange')->name('admin.pages.status.change');

		});

		Route::group(['prefix' => 'media'],function(){

			Route::group(['prefix' => 'files'], function(){

				Route::get('/','backend\MediaController@filesView')->name('admin.media.files.index');
				Route::get('/upload','backend\MediaController@fileUpload')->name('admin.media.file.upload');
				Route::post('/upload/process','backend\MediaController@fileUploadProcess')->name('admin.media.file.upload.process');

			});

			Route::group(['prefix' => 'folder'], function(){

				Route::get('/create','backend\MediaController@folderCreate')->name('admin.media.folder.create');
				Route::post('/create/process','backend\MediaController@folderCreateProcess')->name('admin.media.folder.create.process');
				
			});

		});

		Route::group(['prefix' => 'news'], function(){

			Route::get('/','backend\NewsController@index')->name('admin.news.index');
			Route::get('/create','backend\NewsController@create')->name('admin.news.create');
			Route::post('/create/process','backend\NewsController@createProcess')->name('admin.news.create.process');
			Route::get('/edit/{id}','backend\NewsController@edit')->name('admin.news.edit');
			Route::post('/edit/process','backend\NewsController@editProcess')->name('admin.news.edit.process');
			Route::post('/postnow','backend\NewsController@postNow')->name('admin.news.postnow');
			Route::post('/delete','backend\NewsController@delete')->name('admin.news.delete');

		});

		Route::group(['prefix' => 'bulletin'], function(){

			Route::get('/','backend\BulletinController@index')->name('admin.bulletin.index');
			Route::post('/save','backend\BulletinController@save')->name('admin.bulletin.save');

		});

		Route::group(['prefix' => 'aboutme'], function(){

			Route::get('/','backend\AboutMeController@index')->name('admin.aboutme.index');
			Route::post('/update/process','backend\AboutMeController@update')->name('admin.aboutme.update.process');

		});

		Route::group(['prefix' => 'research'], function(){

			Route::get('/','backend\ResearchController@index')->name('admin.research.index');
			Route::post('/save','backend\ResearchController@save')->name('admin.research.save');

		});

		Route::group(['prefix' => 'business'], function(){

			Route::get('/','backend\BusinessController@index')->name('admin.business.index');
			Route::post('/save','backend\BusinessController@save')->name('admin.business.save');

		});

		Route::group(['prefix' => 'contact'], function(){

			Route::get('/','backend\ContactController@index')->name('admin.contact.index');
			Route::post('/update/process','backend\ContactController@update')->name('admin.contact.update.process');

		});

		Route::group(['prefix' => 'newsletter'], function(){

			Route::get('/subscribers','backend\NewsletterController@subscribers')->name('admin.newsletter.subscribers');

		});

		Route::view('/password/change','auth.passwords.reset')->name('admin.change.password');
		Route::post('/password/change/process','backend\PasswordController@changePasswordProcess')->name('admin.change.password.process');
	
	});

	Route::group(['prefix' => 'newsletter'],function(){
		Route::post('/subscribe','backend\NewsletterController@subscribe')->name('newsletter.subscribe');
		Route::get('/unsubscribe/{email}','backend\NewsletterController@unsubscribe')->name('newsletter.unsubscribe');
	});

	Route::post('contactus/process','backend\EmailController@contactUs');
	Route::post('farmersupport/process','backend\EmailController@farmerSupport');

	Route::redirect('/home','/admin');

	Auth::routes();
