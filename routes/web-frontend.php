<?php

if (env('APP_ENV') === 'production') {
    URL::forceScheme('https');
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'frontend\LandingPageController@index');
Route::get('/page/{slug}','frontend\PagesController@display')->name('page.display');
Route::get('/contactus','frontend\PagesController@contactUs')->name('page.contactUs');
Route::get('/moringa_farmers_support','frontend\PagesController@farmersSupport')->name('page.farmersSupport');
Route::get('/weather','frontend\PagesController@weather')->name('page.weather');
Route::get('/Moringa_Festival','frontend\PagesController@festival')->name('moringa.festival');
Route::get('/Moringa_Gallery','frontend\PagesController@moringa_img')->name('moringa.gallery');
Route::get('/Moringa_Feasibility','frontend\PagesController@feasibility');
Route::get('/Moringa_Seeds','frontend\PagesController@moringa_seeds');




