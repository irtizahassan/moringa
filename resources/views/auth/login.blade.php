{{-- @extends('layouts.app') --}}

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <style>
            *{
                margin: 0;
                padding: 0;
            }
            .page{
                width: 100vw;
                height: 100vh;
                display: flex;
            }
            .left-side{
                height: 100vh;
                width: 65vw;
                background: url(https://www.moringaforlife.co.uk/s/img/emotionheader4954827_3.JPG) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                background-color: black;
                display: flex;
                align-items: center;
                justify-content: center;
                color: white;
            }
            .right-side{
                height: 100vh;
                width: 35vw;
                background-color: #f2f4f8;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            form{
                width: 100%;
                padding: 10%;
            }
            input{
                width: 100%;
                padding: 7px;
                border: 1px #f2f4f8 solid;
                border-radius: 5px;
                margin: 5px;
            }
            label{
                font-size: 1.1rem;
                font-family: sans-serif;
            }

            button{
                width: 100%;
                padding: 7px;
                margin: 5px;
                background-color: green;
            }
            @media only screen and (max-width: 500px)  {
                .page{
                    width: 100%;
                    height: 100%;
                }
                .left-side{
                    height: 100vh;
                    width: 65vw;
                    background: url(https://www.moringaforlife.co.uk/s/img/emotionheader4954827_3.JPG) no-repeat center center fixed; 
                    -webkit-background-size: cover;
                    -moz-background-size: cover;
                    -o-background-size: cover;
                    background-size: cover;
                    background-color: black;
                    display: none;
                }
                .right-side{
                    height: 100%;
                    width: 100%;
                    background-color: #f2f4f8;
                    display: flex;
                    align-items: center;
                    justify-content: center;
                }
                form{
                    width: 100%;
                    padding: 10%;
                }
                input{
                    width: 100%;
                    padding: 7px;
                    border: 1px #f2f4f8 solid;
                    border-radius: 5px;
                    margin: 5px;
                }
                label{
                    font-size: 1.1rem;
                    font-family: sans-serif;
                }

                button{
                    width: 100%;
                    padding: 7px;
                    margin: 5px;
                    background-color: green;
                }
            }
        </style>
    </head>
    <body>
        <div class="page">
            <div class="left-side">
                <h1 style="background-color: rgba(0,0,0,0.4); padding: 7px; border-radius: 5px">Moringa For Life</h1>
            </div>
            <div class="right-side">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <label for="email" class="control-label">E-Mail Address</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="password" class="control-label">Password</label>
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>
                </form>
            </div>
        </div>
    </body>
</html>

{{-- @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 --}}