@extends('backend.layouts.master')
@section('content')

	<style>
		label{
			margin:10px 0;
		}
	</style>

	<div style="width: 100%">
		
		<div class="col-md-8 col-md-offset-2">
			
			<h3 style="text-align: center;">About Business</h3>
			@if (Session::has('message'))
		        <h2 style="text-align: center; padding: 5px" class="btn-success">{{ session('message') }}</h2>
		   	@endif 
		   	@if (Session::has('errormessage'))
		        <h2 style="text-align: center; padding: 5px" class="btn-warning">{{ session('errormessage') }}</h2>
		   	@endif 
			<br>
			<form action="{{url(route('admin.business.save'))}}" method="post">
				{{csrf_field()}}
				
				<label class="col-md-12">Heading: </label>
				<input type="text" name="heading" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="Business Component Heading" value="{{$business['heading']}}" required="">
				<label class="col-md-12">Details: </label>
				<textarea id="editor1" style="" type="text" name="details" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="Details" value="" required="">{{$business['details']}}</textarea>
				<button type="submit" class="btn btn-success" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px; margin:10px 0;">
					<i class="fa fa-floppy-o" aria-hidden="true"></i>
					 Submit
				</button>

			</form>

		</div>

		<div class="clearfix"></div>

	</div>

@endsection

@section('script')

	<script src="http://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>

	<script>
		CKEDITOR.replace('editor1');
	</script>

@endsection