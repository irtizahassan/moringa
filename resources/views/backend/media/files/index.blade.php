@extends('backend.layouts.master')
@section('content')

	<style>
		th,td{
			padding: 10px;
		}
	</style>

	<div style="width: 100%;">
		<div class="pull-right">
			<a class="btn btn-success" href="{{url(route('admin.media.folder.create'))}}">
				<i class="fa fa-plus" aria-hidden="true"></i>
				Create Folder
			</a>			
			<a class="btn btn-info" href="{{url(route('admin.media.file.upload'))}}">
				<i class="fa fa-plus" aria-hidden="true"></i>
				Upload New File
			</a>		
		</div>
		<div class="clearfix"></div>
		<table style="width: 100%" border="1px">
			<thead style="background-color: #2A3F54; color: white">
				<tr>
				<th>Image</th>
					<th style="width: 1%; white-space: nowrap;">Name</th>
					<th>Folder</th>
					<th>URL</th>
					<th style="width: 1%; white-space: nowrap; text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($files as $file)
					<tr>
					<td><img src="{{ url('storage').$file->path }}" width="125px"></td>
						<td style="width: 1%; white-space: nowrap;">{{ $file->name }}</td>
						<td>{{ $file->folder }}</td>
						<td><a href="{{ url('storage').$file->path }}" target="_blank">{{ url('storage').$file->path }}</a></td>
						<td style="text-align: center;">
							<a href="{{ url('storage').$file->path }}" class="btn btn-info" target="_blank" style="font-size: 10px">
								<i class="fa fa-eye" aria-hidden="true"></i>
								View
							</a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection

@section('script')



@endsection