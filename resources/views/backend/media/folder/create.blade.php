@extends('backend.layouts.master')
@section('content')

	<style>
		label{
			margin:10px 0;
		}
	</style>

	<div style="width: 100%">
		
		<a class="btn btn-info pull-left" href="{{url(route('admin.media.files.index'))}}">
			<i class="fa fa-arrow-left" aria-hidden="true"></i>
				Back To Files
		</a>

		<div class="clearfix"></div>		
		
		<div class="col-md-4 col-md-offset-4">
			
			<h3 style="text-align: center;">Create New Folder</h3>
			@if (Session::has('message'))
		        <h2 style="text-align: center; padding: 5px" class="btn-success">{{ session('message') }}</h2>
		   	@endif 
		   	@if (Session::has('errormessage'))
		        <h2 style="text-align: center; padding: 5px" class="btn-warning">{{ session('errormessage') }}</h2>
		   	@endif 
			<br>
			<form action="{{url(route('admin.media.folder.create.process'))}}" method="post" enctype="multipart/form-data">
				{{csrf_field()}}
				<label class="col-md-12">Folder Name</label>
				<input type="text" name="name" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="Folder Name" required="">
				<input type="submit" class="btn btn-success" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px; margin:10px 0;">
			</form>

		</div>

		<div class="clearfix"></div>

	</div>

@endsection