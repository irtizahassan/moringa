@extends('backend.layouts.master')
@section('content')

	<div style="width: 100%">
		
		<div class="col-md-12">
			<h3 style="text-align: center;">Bulletin</h3>
			<div id="bulletin-container" style="margin: 10px 0">
				
			</div>
		</div>
		<div class="pull-right" style="margin: 15px;">
			<button class="btn btn-success" id="newBulletinItem">Add New</button>
			<button class="btn btn-success" id="saveBulletin">Save</button>
		</div>
		<div class="clearfix"></div>

	</div>

@endsection

@section('style')

	<style>
		.bulletin-item{
			margin: 5px;
			border: 1px #424242 solid;
		}
		.bulletin-item-heading{
			padding: 10px;
			background-color:white;
		}
		.bulletin-item-content{
			padding: 10px;
			background-color: #888;
		}
	</style>

@endsection

@section('script')

	<script>

		$(document).ready(function(){
			var bulletin = @php echo json_encode($bulletin); @endphp;

			bulletin.forEach(function(item,index){
				$('#bulletin-container').append(bulletinItem(item.heading,item.url));
			});

		});
			
		$('#newBulletinItem').on('click',function(){

			addNewBulletinItem();

		});

		function addNewBulletinItem(){
	
			item = bulletinItem("","");

			$('#bulletin-container').append(item);

		}

		function bulletinItem(heading,url){
			
			html = '<div class="row bulletin-item" style="margin: 10px 0">';
			html += '<div class="col-md-6 bulletin-item-heading">';
			html += '<input type="text" class="heading" style="width:100%; border-radius: 5px; color: black; padding: 3px" placeholder="Bulletin Heading Here" value="'+heading+'">';
			html += '</div>';
			html += '<div class="col-md-6 bulletin-item-content">';
			html += '<input type="text" class="url" style="width:100%; border-radius: 5px; color: black; padding: 3px" placeholder="Bulletin URL Here" value="'+url+'">';
			html += '</div>';
			html += '</div>';

			return html;

		}

		$('#saveBulletin').on('click', function(){
			var save = $(this);
			var addNew = $('#newBulletinItem');
			var inputs = $('input');

			var bulletinArray = {};

			$('#bulletin-container .bulletin-item').each(function(){
				
				var bullet = [];
				bullet['heading'] = $(this).find(".heading").val();
				bullet['url'] = $(this).find(".url").val();

				if(!((bullet['heading']).trim() == ""))
					bulletinArray[bullet['heading']] = bullet['url'];
			
			});

			if((bulletinArray).length != 0){

				inputs.prop('disabled',true);
				addNew.prop('disabled',true);
				save.prop('disabled',true);

				$.ajax({
					type: 'POST',
					url: '{{url(route('admin.bulletin.save'))}}',
					data: {_token:'{{csrf_token()}}',bulletin:JSON.stringify(bulletinArray)},
					success: function(response){
						location.reload();
					}
				});

			}

		});

		function objectLength(obj) {
		  	var result = 0;
		  	for(var prop in obj) {
		    	if (obj.hasOwnProperty(prop)) {
		      		result++;
		    	}
		  	}
		  	return result;
		}


	</script>

@endsection