@extends('backend.layouts.master')
@section('content')

<h2 style="text-align: center;">Create New Page</h2>

<div class="" style="display:flex; justify-content: center; text-align: center; width: 100%">
	
	<form action="{{ url('admin/pages/create/process') }}" method="post" accept-charset="utf-8">
		{{ csrf_field() }}
		<div class="form-group">
	      <label class="control-label col-md-12" style="text-align: left;">Title</label>
	      <div class="col-md-12" style="margin-bottom: 10px;">
	        <input type="text" class="form-control" name="title" placeholder="Title Here" required="">
	      </div>
	      <label class="control-label col-md-12" style="text-align: left;">Content</label>
	      <div class="col-md-12" style="margin-bottom: 10px;">
	        <textarea style="height: 400px;" name="content" id="content" class="form-control" style="width: 100%" required=""></textarea>
	      </div>
	    </div>
	    <button type="submit" class="btn btn-success pull-right">
	    	<i class="fa fa-floppy-o" aria-hidden="true"></i>
			Create Page
	    </button>
	</form>

</div>	

@endsection