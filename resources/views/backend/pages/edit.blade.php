@extends('backend.layouts.master')
@section('content')

<h2 style="text-align: center;">Edit Page</h2>

<div class="" style="display:flex; justify-content: center; text-align: center; width: 100%">
	
	<form action="{{ url('admin/pages/edit/process') }}" method="post" accept-charset="utf-8">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{$page->id}}"></input>
		<div class="form-group">
	      <label class="control-label col-md-12" style="text-align: left;">Title</label>
	      <div class="col-md-12" style="margin-bottom: 10px;">
	        <input type="text" class="form-control" name="title" placeholder="Title Here" required="" value="{{$page->title}}">
	      </div>
	      <label class="control-label col-md-12" style="text-align: left;">Slug</label>
	      <div class="col-md-12" style="margin-bottom: 10px;">
	        <input type="text" class="form-control" name="slug" placeholder="Slug means page access url" required="" value="{{$page->slug}}">
	      </div>
	      <label class="control-label col-md-12" style="text-align: left;">Content</label>
	      <div class="col-md-12" style="margin-bottom: 10px;">
	        <textarea style="height: 400px;" name="content" id="content" class="form-control" style="width: 100%" required="">{{$page->content}}</textarea>
	      </div>
	    </div>
	    <input type="submit" class="btn btn-success pull-right" value="Save Edit"></input>
	</form>

</div>	

@endsection