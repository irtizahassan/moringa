@extends('backend.layouts.master')
@section('content')

<style>
	th{
		text-align: center;
		padding: 10px;
		background-color: #424242;
		color:white;
	}
	td{
		padding: 5px;
	}
	.nospace{
		width: 1%;
		white-space: nowrap;
	}
</style>

<div class="">
	
	<a href="{{ url('admin/pages/create') }}" class="btn btn-success pull-right">
		<i class="fa fa-plus" aria-hidden="true"></i>
			Create New Page
	</a>

	<div class="clearfix"></div>

	<table border="1" width="100%">
		<tr>
			<th class="nospace">Sr#</th>
			<th>Title</th>
			<th class="nospace">Created At</th>
			<th class="nospace">Last Modified</th>
			<th colspan="5" class="nospace">Action</th>
		</tr>
		@foreach($pages as $page)
			<tr>
				<td style="text-align: center;" class="nospace">
					{{ $loop->iteration }}
				</td>
				<td>
					{{ $page->title }}
				</td>
				<td style="text-align: center;" class="nospace">
					{{ explode(' ',$page->created_at)[0] }}
				</td>
				<td style="text-align: center;" class="nospace">
					{{ explode(' ',$page->updated_at)[0] }}
				</td>
				<td style="text-align: center;">
					<a href="{{url(route('page.display',$page->slug))}}"  target="_blank" class="btn btn-info" style="font-size: 10px">
						<i class="fa fa-eye" aria-hidden="true"></i>
						View
					</a>
				</td>
				<td style="text-align: center;">
					<a href="{{url('admin/pages/edit',$page->id)}}" class="btn btn-warning" style="font-size: 10px">
						<i class="fa fa-pencil" aria-hidden="true"></i>
						Edit
					</a>
				</td>
				<td style="text-align: center;">
					<a class="btn btn-warning" style="font-size: 10px">
						<i class="fa fa-pencil" aria-hidden="true"></i>
						Edit Inline
					</a>
				</td>
				<td style="text-align: center;">
					<a href="{{url('admin/pages/delete',$page->id)}}" class="btn btn-danger" style="font-size: 10px">
						<i class="fa fa-trash" aria-hidden="true"></i>
						Delete
					</a>
				</td>
				<td style="text-align: center;">
					<form action="{{url(route('admin.pages.status.change'))}}" method="post" >
						{{csrf_field()}}
						<input type="hidden" name="id" value="{{$page->id}}">
						<button type="submit" style="font-size: 10px" class="btn btn-primary">
							@php
								echo ($page->published == 0 ? 
									'
										<i class="fa fa-check" aria-hidden="true"></i>
											Publish
									' 
									: 
									'
										<i class="fa fa-times" aria-hidden="true"></i>
											UnPublish
									');
							@endphp
							

						</button>
					</form>
				</td>
			</tr>
		@endforeach

	</table>
</div>

@endsection