@extends('backend.layouts.master')
@section('content')

	<style>
		label{
			margin:10px 0;
		}
	</style>

	<div style="width: 100%">
		
		<div class="col-md-12">
			
			<h3 style="text-align: center;">Newsletter Subscribers</h3>
			<br>
			
			@foreach($subscribers as $subscriber)

				<div class="btn-default" style="width: 100%; text-align: left; padding: 15px; font-size: 1.6rem">
					Email: ({{$subscriber->email}}), &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					Since: ({{date('d/m/Y', strtotime($subscriber->updated_at))}})
				</div>

			@endforeach

		</div>

		<div class="clearfix"></div>

	</div>

@endsection