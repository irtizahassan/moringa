
<!DOCTYPE html>
	<head>
	    
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="google-site-verification" content="jtIRmhZEP17H0mSSc1xheF9a6_LRuJtQrj_XxKskwZo" />

	    <title>Moringa Admin </title>

	    <link href="{{URL::to('admin_backend/css/bootstrap.min.css')}}" rel="stylesheet">
	    <link href="{{URL::to('admin_backend/fonts/css/font-awesome.min.css')}}" rel="stylesheet">
	    <link href="{{URL::to('admin_backend/css/custom.css')}}" rel="stylesheet">

	    <style>
	    	.ephox-polish-source-container,#ephox_content{
	    		min-width: 75vw !important;
	    	}
	    	.CodeMirror-code{
	    		text-align: left;
	    	}
	    </style>

	    @yield('style')

	</head>

	<body class="nav-md">

		<div class="container body">

    		<div class="main_container">

        		<div class="col-md-3 left_col">
            		<div class="left_col scroll-view">

		                <div class="navbar nav_title" style="border: 0;">
		                    <a href="{{url('/admin')}}" class="site_title"><i class="fa fa-dashboard"></i> <span>Moringa For Life</span></a>
		                </div>
		                <div class="clearfix"></div>

		                <!-- menu prile quick info -->
		                <div class="profile">
		                    <div class="profile_pic">
		                        <img src="{{URL::to('admin_backend/images/user.png')}}" alt="..." class="img-circle profile_img">
		                    </div>
		                    <div class="profile_info">
		                        <span>Welcome,</span>
		                        <h2>{{ Auth::user()->name }}</h2>
		                    </div>
		                </div>

                		<br />

		                <!-- sidebar menu -->
		                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

		                    <div class="menu_section">
		                        <h3>&nbsp;</h3>
		                        <ul class="nav side-menu">
		                            <li>
		                                <a href="{{url('/admin')}}">
		                                    <i class="fa fa-home"></i> Dashboard</a>
		                            </li>
		                            <li>
		                            	<a>
		                            		<i class="fa fa-bars" aria-hidden="true"></i>
											Menu
		                            	</a>
		                            	<ul class="nav child_menu" style="display: none">
		                                    <li>
		                                        <a href="{{url(route('admin.menu.index'))}}">
		                                            <i class="fa fa-eye"></i> View
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.menu.item.create'))}}">
		                                            <i class="fa fa-plus" aria-hidden="true"></i>
		                                            Add New Item
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.menu.sort'))}}">
		                                            <i class="fa fa-sort" aria-hidden="true"></i>
		                                            ReArrange Items
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li>
		                            <li>
		                                <a>
		                                    <i class="fa fa-file"></i> Pages
		                                </a>
		                                <ul class="nav child_menu" style="display: none"> 
		                                    <li>
		                                        <a href="{{url('/admin/pages/create')}}">
		                                            <i class="fa fa-plus" aria-hidden="true"></i>
		                                            Create New
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url('/admin/pages')}}">
		                                            <i class="fa fa-eye"></i> View All
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li>
		                            <li>
		                            	<a>
		                            		<i class="fa fa-picture-o" aria-hidden="true"></i>
											Media
		                            	</a>
		                            	<ul class="nav child_menu" style="display: none"> 
		                                    <li>
		                                        <a href="{{url(route('admin.media.files.index'))}}">
		                                            <i class="fa fa-eye" aria-hidden="true"></i>
		                                            View Files
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.media.file.upload'))}}">
		                                            <i class="fa fa-upload" aria-hidden="true"></i>
		                                            Upload File
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.media.folder.create'))}}">
		                                            <i class="fa fa-plus"></i> 
		                                            Create Folder
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li>
		                            {{-- <li>
		                            	<a>
		                            		<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
											Blog
		                            	</a>
		                            	<ul class="nav child_menu" style="display: none"> 
		                            		<li>
		                                        <a href="#">
		                                            <i class="fa fa-pencil" aria-hidden="true"></i>
		                                            Write Post
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="#">
		                                            <i class="fa fa-eye" aria-hidden="true"></i>
		                                            View All Posts
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li> --}}
		                            <li>
		                            	<a>
		                            		<i class="fa fa-newspaper-o" aria-hidden="true"></i>
											News
		                            	</a>
		                            	<ul class="nav child_menu" style="display: none"> 
		                            		<li>
		                                        <a href="{{ url(route('admin.news.create')) }}">
		                                            <i class="fa fa-plus" aria-hidden="true"></i>
		                                            Add New
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{ url(route('admin.news.index')) }}">
		                                            <i class="fa fa-eye" aria-hidden="true"></i>
		                                            View All
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li>
		                            <li>
		                            	<a href="{{url(route('admin.bulletin.index'))}}">
		                            		<i class="fa fa-dot-circle-o" aria-hidden="true"></i>
											Bulletin
		                            	</a>
		                            </li>
		                            <li>
		                            	<a>
		                            		<i class="fa fa-picture-o" aria-hidden="true"></i>
											Main Page
		                            	</a>
		                            	<ul class="nav child_menu" style="display: none"> 
		                                    <li>
		                                        <a href="{{url(route('admin.aboutme.index'))}}">
		                                            <i class="fa fa-user" aria-hidden="true"></i>
		                                            About Me
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.research.index'))}}">
		                                            <i class="fa fa-home" aria-hidden="true"></i>
		                                            Research
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.business.index'))}}">
		                                            <i class="fa fa-facebook" aria-hidden="true"></i>
		                                            About Business
		                                        </a>
		                                    </li>
		                                    <li>
		                                        <a href="{{url(route('admin.contact.index'))}}">
		                                            <i class="fa fa-user" aria-hidden="true"></i>
		                                            Contact Info
		                                        </a>
		                                    </li>
		                                </ul>
		                            </li>
		                            <li>
	                                    <a href="{{url(route('admin.newsletter.subscribers'))}}">
	                                        <i class="fa fa-user" aria-hidden="true"></i>
	                                        Subscribers
	                                    </a>
	                                </li>
		                        </ul>
		                    </div>
		                </div>
		                <!-- /sidebar menu -->
		            </div>
		        </div>

		        <!-- top navigation -->
		        <div class="top_nav">

		            <div class="nav_menu">
		                <nav class="" role="navigation">
		                    <div class="nav toggle">
		                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
		                    </div>

		                    <ul class="nav navbar-nav navbar-right">
		                        <li class="">
		                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		                                <img src="{{URL::to('admin_backend/images/user.png')}}" alt="">{{Auth::user()->name}}
		                                <span class=" fa fa-angle-down"></span>
		                            </a>
		                            <ul class="dropdown-menu dropdown-usermenu pull-right">
		                                <li style="cursor: pointer;">
		                                	<a href="{{ url(route('admin.change.password')) }}">
		                                        <i class="fa fa-unlock-alt pull-right"></i>
		                                         Change Password
		                                    </a>
		                                    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
		                                        <i class="fa fa-sign-out pull-right"></i>
		                                         Log Out
		                                    </a>
		                                </li>
		                            </ul>
		                        </li>
		                        <li>
		                    		<a href="{{url('/')}}" class="user-profile" target="_blank">
		                                Visit Site
		                            </a>
		                    	</li>
		                    </ul>
		                </nav>
		            </div>

		        </div>
		        <!-- /top navigation -->

		        <!-- page content -->
		        <div class="right_col" role="main">

					<div style="padding: 15px;">
						
			        	@yield('content')

					</div>
		            
		            <!-- footer content -->

		            <footer>
		                <div class="copyright-info">
		                    <p class="pull-right">Moringa for life Admin Panel</p>
		                </div>
		                <div class="clearfix"></div>
		            </footer>

		        </div>
		        <!-- /page content -->

    		</div>

		</div>

		<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
		    {{ csrf_field() }}
		</form>
    
    	<script src="{{URL::to('admin_backend/js/jquery.min.js')}}"></script>
		<script src="{{URL::to('admin_backend/js/bootstrap.min.js')}}"></script>
		<script src="{{URL::to('admin_backend/js/nicescroll/jquery.nicescroll.min.js')}}"></script>
		<script src="{{URL::to('admin_backend/js/custom.js')}}"></script>
		<script src="{{URL::to('admin_backend/js/pace/pace.min.js')}}"></script>

		@if(isset($hasEditor))
			<script src="https://cdn.ckeditor.com/4.7.3/full-all/ckeditor.js"></script>
	    
		    <script>

		        $(document).ready(function(){
		            CKEDITOR.replace( 'content',{
		            	filebrowserBrowseUrl: '{{ URL::to('ckfinder/ckfinder.html') }}',
				        filebrowserUploadUrl: '{{ URL::to('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
		            } );
		        });

		    </script>
		@endif

		@yield('script')

	</body>

</html>