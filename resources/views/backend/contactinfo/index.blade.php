@extends('backend.layouts.master')
@section('content')

	<style>
		label{
			margin:10px 0;
		}
	</style>

	<div style="width: 100%">

		<div class="col-md-4 col-md-offset-4">

			<h3 style="text-align: center;">Edit Contact Information</h3>
			@if (Session::has('message'))
		        <h2 style="text-align: center; padding: 5px" class="btn-success">{{ session('message') }}</h2>
		   	@endif
		   	@if (Session::has('errormessage'))
		        <h2 style="text-align: center; padding: 5px" class="btn-warning">{{ session('errormessage') }}</h2>
		   	@endif
			<br>
			<form action="{{url(route('admin.contact.update.process'))}}" method="post">
				{{csrf_field()}}
				<label class="col-md-12"><i class="fa fa-phone" aria-hidden="true"></i> Phone No: </label>
				<input type="text" name="phone" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="Phone No" value="{{ $contactInfo['phone'] }}">
				<label class="col-md-12"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email: </label>
				<input type="text" name="email" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="email@domain.com" value="{{ $contactInfo['email'] }}">
				<label class="col-md-12"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email2: </label>
				<input type="text" name="email2" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="email@domain.com" value="{{ $contactInfo['email2'] }}">
				<label class="col-md-12"><i class="fa fa-home" aria-hidden="true"></i> Address: </label>
				<input type="text" name="address" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px;" placeholder="Address" value="{{ $contactInfo['address'] }}">
				<button type="submit" class="btn btn-success" style="width: 100%; padding: 6px; border:1px #424242 solid; border-radius: 4px; margin:10px 0;">
					<i class="fa fa-floppy-o" aria-hidden="true"></i>
					 Submit
				</button>

			</form>

		</div>

		<div class="clearfix"></div>

	</div>

@endsection