@extends('backend.layouts.master')
@section('content')

	<style>
		th,td{
			padding: 10px;
		}
	</style>

	<div style="width: 100%;">
		<div class="pull-right">
			<a class="btn btn-success" href="{{url(route('admin.news.create'))}}">
				<i class="fa fa-plus" aria-hidden="true"></i>
				Create
			</a>		
		</div>
		<div class="clearfix"></div>
		<table style="width: 100%" border="1px">
			<thead style="background-color: #2A3F54; color: white">
				<tr>
					<th>News</th>
					<th>Posted</th>
					<th>Scheduled</th>
					<th>Created At</th>
					<th colspan="3" style="width: 1%; white-space: nowrap; text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($news as $item)
					<tr>
						<td>{{ $item->content }}</td>
						<td style="width: 1%; white-space: nowrap; text-align: center;">
							{{ ($item->posted == 0) ? "No" : "Yes" }}
						</td>
						<td style="width: 1%; white-space: nowrap; text-align: center;">
							@if($item->time == 0)
								{{ "Direct Posted" }}
							@else
								{!! str_replace(" ","<br>",date('d/m/Y h:i:s',$item->time)) !!}
							@endif
						</td>
						<td style="width: 1%; white-space: nowrap; text-align: center;">
							{!! str_replace(" ","<br>",($item->created_at)) !!}
						</td>
						<td>
							@if($item->posted == 0)
								<form action="{{ url(route('admin.news.postnow')) }}" method="post">
								
									{{ csrf_field() }}
									<input type="hidden" name="id" value="{{ $item->id }}">
									<button type="submit" class="btn btn-success" style="font-size: 10px;">
										<i class="fa fa-plus" aria-hidden="true"></i>
										Post Now
									</button>

								</form>
							@endif
						</td>
						<td>
							<a href="{{url(route('admin.news.edit',$item->id))}}" class="btn btn-warning" style="font-size: 10px;">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Edit
							</a>
						</td>
						<td style="text-align: center;">
							<form action="{{ url(route('admin.news.delete')) }}" method="post">
								
								{{ csrf_field() }}
								<input type="hidden" name="index" value="{{ $item->id }}">
								<button type="submit" class="btn btn-danger" style="font-size: 10px;">
									<i class="fa fa-trash" aria-hidden="true"></i>
									Delete
								</button>

							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection