@extends('backend.layouts.master')
@section('content')

	<style>
		th,td{
			padding: 10px;
		}
	</style>

	<div style="width: 100%;">
		<div class="pull-right">
			<a class="btn btn-success" href="{{url(route('admin.menu.item.create'))}}">
				<i class="fa fa-plus" aria-hidden="true"></i>
				Add New Item
			</a>			
			<a class="btn btn-info" href="{{url(route('admin.menu.sort'))}}">
				<i class="fa fa-sort" aria-hidden="true"></i>
				Change Order
			</a>		
		</div>
		<div class="clearfix"></div>
		<table style="width: 100%" border="1px">
			<thead style="background-color: #2A3F54; color: white">
				<tr>
					<th style="width: 1%; white-space: nowrap;">Name</th>
					<th>URL</th>
					<th colspan="3" style="width: 1%; white-space: nowrap; text-align: center;">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($menuItems as $key => $item)
					<tr>
						<td style="width: 1%; white-space: nowrap;">{{ $item->name }}</td>
						<td>{{ $item->url }}</td>
						<td style="text-align: center;">
							<a href="{{ url($item->url) }}" class="btn btn-info" style="font-size: 10px;">
								<i class="fa fa-eye" aria-hidden="true"></i>
								View
							</a>
						</td>
						<td style="text-align: center;">
							<a href="{{ url(route('admin.menu.item.edit',$loop->index)) }}" class="btn btn-warning" style="font-size: 10px;">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								Edit
							</a>
						</td>
						<td style="text-align: center;">
							<form action="{{ url(route('admin.menu.item.delete.process')) }}" method="post">
								
								{{ csrf_field() }}
								<input type="hidden" name="index" value="{{ $key }}">
								<button type="submit" class="btn btn-danger" style="font-size: 10px;">
									<i class="fa fa-trash" aria-hidden="true"></i>
									Delete
								</button>

							</form>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@endsection