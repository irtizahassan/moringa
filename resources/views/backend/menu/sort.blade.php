@extends('backend.layouts.master')
@section('content')

	<style>
		label{
			margin:10px 0;
		}
		th,td{
			padding: 10px;
		}
		tr.placeholder{
			position: relative;
		    margin: 0;
		    padding: 0;
		    border: none;
		}
		tr.placeholder:before{
			position: relative;
	      	content: "";
	      	width: 0;
	      	height: 0;
	      	font-size: 3rem;
	      	margin: 5px;
	      	left: 0px;
	      	top: 0px;
	      	padding: 5px;
	      	/*border: 1px solid transparent;*/
	      	/*border-left-color: red;*/
	      	border-right: none;
		}

		body.dragging, body.dragging * {
		  cursor: move !important;
		}

		.dragged {
		  position: absolute;
		  opacity: 0.5;
		  z-index: 2000;
		}
	</style>

	<div style="width: 100%">
		
		<a class="btn btn-info" href="{{url(route('admin.menu.index'))}}">
			<i class="fa fa-arrow-left" aria-hidden="true"></i>
				Back To Menu
		</a>

		<h2>Re Order Menu Items</h2>

		<table style="width: 100%" border="1px" class="sorted_table">
			<thead style="background-color: #2A3F54; color: white">
				<tr>
					<th style="width: 1%; white-space: nowrap;">Name</th>
					<th>URL</th>
				</tr>
			</thead>
			<tbody>
				@foreach($menuItems as $item)
					<tr>
						<td class="name" style="width: 1%; white-space: nowrap;">{{ $item['name'] }}</td>
						<td class="url">{{ $item['url'] }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<button class="btn btn-success pull-right" id="saveButton" style="margin:10px 0">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>
			 Save Changes
		</button>
		
		<div class="clearfix"></div>
	</div>

@endsection

@section('script')
	
	<script src="{{URL::to('admin_backend/js/jquery_sortable.js')}}"></script>

	<script>
		
		var sortableList = $('.sorted_table').sortable({
		  containerSelector: 'table',
		  itemPath: '> tbody',
		  itemSelector: 'tr',
		  placeholder: '<tr class="placeholder"/>'
		});

		$("#saveButton").on("click",function (){

			var button = $(this);

			button.prop("disabled",true);

			var data = [];

			var table = $('.sorted_table tbody tr').each(function(){

				var name = $(this).find(".name").html();
				var url  = $(this).find(".url").html();

				data.push({name:name,url:url});

			});

			saveChanges(data);

		});

		function saveChanges(menuarray){

			$.ajax({

				method: "post",
				url: "{{url(route('admin.menu.sort.process'))}}",
				data: {menu:menuarray,_token:"{{csrf_token()}}"},
				success: function(response){
					window.location.href = response;
				},
				error: function(){

				}

			});

		}

	</script>

@endsection