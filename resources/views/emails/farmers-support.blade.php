<html>
<head>
	<title>MoringaForLife Farmers Support Message Received</title>
</head>
<body style="background: rgb(234, 236, 237); color: black">
	<table style="background-color: white; width: 50%; margin: auto">
		<tr>
			<td style="text-align: center; font-family: cursive; padding: 20px" colspan="2">
				<h2>MoringaForLife.pk</h2>
				<h2>Farmers Support Form</h2>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<h4>News Message Received</h4>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; font-family: inherit; padding: 20px; width: 1%;">
				<p style="margin-bottom: 0"><strong>Name: </strong></p>
			</td>
			<td style="text-align: left;">
				<p style="margin-bottom: 0;">{{$name}}</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; font-family: inherit; padding: 20px; width: 1%;">
				<p style="margin-bottom: 0"><strong>Email: </strong></p>
			</td>
			<td style="text-align: left;">
				<p style="margin-bottom: 0;">{{$email}}</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; font-family: inherit; padding: 20px; width: 1%;">
				<p style="margin-bottom: 0"><strong>Phone: </strong></p>
			</td>
			<td style="text-align: left;">
				<p style="margin-bottom: 0;">{{$phone}}</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; font-family: inherit; padding: 20px; width: 1%;">
				<p style="margin-bottom: 0"><strong>Message: </strong></p>
			</td>
			<td style="text-align: left;">
				<p style="margin-bottom: 0; word-wrap: break-word; max-width: 300px;">{{$messager}}</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<hr>
			</td>
		</tr>
	</table>
</body>
</html>