<html>
<head>
	<title>MoringaForLife Newsletter Subscription successfull</title>
</head>
<body style="background: rgb(234, 236, 237); color: black">
	<table style="background-color: white; width: 50%; margin: auto">
		<tr>
			<td style="text-align: center; font-family: cursive; padding: 20px">
				<h2>MoringaForLife.pk</h2>
			</td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: inherit; padding: 20px">
				<h4>News Letter Subscription Successfull</h4>
				<p style="margin-bottom: 0"><strong>Thankyou</strong> for subscribing to our newsletter.</p>
				<p style="margin: 0">You will receive all the important news via email.</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: center; font-family: inherit; padding: 20px">
				<h6>MoringaForLife.pk | <a href="{{url(route('newsletter.unsubscribe',$email))}}" target="_blank">UnSubscribe</a></h6>
			</td>
		</tr>
	</table>
</body>
</html>