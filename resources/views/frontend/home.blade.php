@extends('layouts.frontend.base')
@section('body')

    <div class="loading text-center vertical-middle">
      <div class="row"><img src="assets/images/loader.gif" width="60" alt="Loader logo" class="loader"/><img src="assets/images/logo/logo-white.png" width="160" alt="Loader logo" class="loader-logo"/>
      </div>
    </div>
    <header class="main-navigation" style="background-color: rgba(0,0,0,0.5);">
      <div class="container-fluid">
        <div class="col-md-2"><a href="home.html" class="logo"><img src="assets/images/logo/logo-white.png" alt="Logo"/></a>
          <div id="navicon">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
          </div>
        </div>
        <div class="col-md-10">
          <nav>
            <ul role="menu">
              <li class="dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"> HOME <span class="dropdown-icon">
                    <svg width="10" height="7">
                      <g></g>
                      <line fill="none" stroke="#fdfdfd" x1="-4.54458" y1="-5.33302" x2="5.38279" y2="4.59435"></line>
                      <line fill="none" stroke="#fdfdfd" x1="4.67324" y1="4.59752" x2="14.81695" y2="-5.54619"></line>
                    </svg></span></a>
                <ul class="dropdown-menu">
                  <li><a href="">SLIDER</a></li>
                  <li><a href="">VIDEO</a></li>
                  <li><a href="">PARALLAX</a></li>
                  <li><a href="">FULLSCREEN</a></li>
                </ul>
              </li>
              <li class="dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"> PROJECTS <span class="dropdown-icon">
                    <svg width="10" height="7">
                      <g></g>
                      <line fill="none" stroke="#fdfdfd" x1="-4.54458" y1="-5.33302" x2="5.38279" y2="4.59435"></line>
                      <line fill="none" stroke="#fdfdfd" x1="4.67324" y1="4.59752" x2="14.81695" y2="-5.54619"></line>
                    </svg></span></a>
                <ul class="dropdown-menu">
                  <li><a href="">MASONRY</a></li>
                </ul>
              </li>
              <li><a href="">SERVICES</a></li>
              <li class="dropdown"><a data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"> BLOG <span class="dropdown-icon">
                    <svg width="10" height="7">
                      <g></g>
                      <line fill="none" stroke="#fdfdfd" x1="-4.54458" y1="-5.33302" x2="5.38279" y2="4.59435"></line>
                      <line fill="none" stroke="#fdfdfd" x1="4.67324" y1="4.59752" x2="14.81695" y2="-5.54619"></line>
                    </svg></span></a>
                <ul class="dropdown-menu">
                  <li><a href="">BLOG</a></li>
                  <li><a href="">BLOG POST</a></li>
                  <li><a href="">SIDEBAR</a></li>
                </ul>
              </li>
              <li><a href="contact.html">CONTACT</a></li>
            </ul>
          </nav>
        </div>
        <div class="mobile-navigation vertical-middle text-center">
          <ul role="menu" class="text-center">
            <li><a href="#!" class="mobile-dropdown">HOME</a>
              <div class="content"><a href="home.html">SLIDER</a><a href="home-video.html">VIDEO</a><a href="home-parallax.html">PARALLAX</a><a href="home-fullscreen.html">FULLSCREEN</a></div>
            </li>
            <li><a href="about.html">AVOIR</a></li>
            <li><a href="#!" class="mobile-dropdown">PROJECTS</a>
              <div class="content"><a href="portfolio.html">MASONRY</a><a href="portfolio-grid.html">GRID</a><a href="portfolio-single.html">PROJECT POST</a></div>
            </li>
            <li><a href="services.html">SERVICES</a></li>
            <li><a href="#!" class="mobile-dropdown">BLOG</a>
              <div class="content"><a href="blog.html">BLOG</a><a href="blog-post.html">BLOG POST</a></div>
            </li>
            <li><a href="contact.html">CONTACT</a></li>
          </ul>
        </div>
      </div>
    </header>
    <div id="home-slider">
      <div class="rev_slider_wrapper header-introduction text-center">
        <div id="rev_slider_1" data-version="5.0" class="rev_slider">
          <ul>
            <li data-transition="slideoverup"><img src="{{ URL::to('uploaded/images/img6.jpeg') }}" alt="Slider Image" width="1920" height="1000"/>
              <div data-x="center" data-y="center" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:50;opacity:0s:400;s:400;e:Power2.easeInOut;" data-transform_out="y:+350;opacity:0s:400;s:400;e:Power2.easeInOut;" data-start="500" class="tp-caption caption-classic">
                <h2>Moringa</h2>
              </div>
            </li>
            <li data-transition="slideoverup"><img src="{{ URL::to('uploaded/images/img1.jpg') }}" alt="Slider Image" width="1920" height="1000"/>
              <div data-x="center" data-y="center" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:50;opacity:0s:400;s:400;e:Power2.easeInOut;" data-transform_out="x:-150;opacity:0s:400;s:400;e:Power2.easeInOut;" data-start="500" class="tp-caption caption-bordered"><span></span>
                <h2>WELCOME</h2><a href="portfolio.html" class="btn btn-link">PORTFOLIO</a>
              </div>
            </li>
            <li data-transition="slideoverup"><img src="{{ URL::to('uploaded/images/img5.jpeg') }}" alt="Slider Image" width="1920" height="700"/>
              <div data-x="center" data-y="middle" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:50;opacity:0s:400;s:400;e:Power2.easeInOut;" data-transform_out="opacity:0s:400;s:400;e:Power2.easeInOut;" data-start="500" class="tp-caption caption-classic">
                <h2>AMAZING</h2><img src="assets/images/section-decor-white.png" width="40" alt="Divider"/>
               <p></p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <section class="section section-white">
      <div class="container-fluid">
        <div class="col-md-10 col-md-offset-1">
          <ul class="masonry-wrap">
            <li data-os-animation="fadeIn" data-os-animation-delay="0s" class="col-lg-12 os-animation">
              <div class="masonry-item background-color-2">
                <img src="{{ URL::to('uploaded/images/img11.png') }}" alt="Portfolio-image"/>
                <div class="masonry-item-overlay">
                  <div class="masonry-item-overlay-inner">
                    <h2>Products</h2>
                    <p>Products Generic Description</p>
                    <a class="btn btn-white">View All</a>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <section class="section section-semi-white">
      <div class="col-md-12 text-center section-head padding-bottom"><small>-----</small>
        <h3>Moringa Oleifera - Introduction</h3>
        <p style="text-align: left">Moringa Olefera is native to Pakistan  and India now found through out the tropics. . It is considered as miracle plant due to its nutritional benefits. Almost all parts of the Moringa oleifera tree can be eaten or used as ingredients in traditional herbal medicines.
          Moringa Oleifera is fast growing tree native to south Asia and now found through out the tropics....</p>
        <a class="btn btn-black">Read More</a>
      </div>
       <div class="container">
        {{-- <div class="col-md-12 padding-top text-center"><a class="btn btn-link">FOLLOW US </a></div> --}}
      </div>
    </section>  
    <section style="background-image: url({{ URL::to('uploaded/images/img4.jpeg') }})" class="section section-black background-image background-image-center">
      <div class="container banner">
        <div class="col-md-12 text-center"><span style="margin-bottom: 10px;">Our Story</span>
          <h2 style="font-variant-caps: small-caps;">Moringa For Life</h2><img src="assets/images/section-decor-white.png" width="40" alt="Divider"/>
          <p>Small Description about how this business came to be and what the objectives are...<br>
          further detail will be provided on the next page</p><a class="btn btn-white">Read More</a>
        </div>
      </div>
      
    </section>

    <section class="section section-semi-white">
      <div class="col-md-12 text-center section-head padding-bottom"><small>-----</small>

      </div>
      <div class="row-fluid">
        <ul class="instagram-widget">
          <li class="col-md-3 col-sm-6 col-xs-6">
            <div class="row"><a href="#!"><img src="{{ URL::to('uploaded/images/img7.jpg') }}" alt="Widget Image"/></a></div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6">
            <div class="row"><a href="#!"><img src="{{ URL::to('uploaded/images/img12.jpg') }}" alt="Widget Image"/></a></div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6">
            <div class="row"><a href="#!"><img src="{{ URL::to('uploaded/images/img2.jpg') }}" alt="Widget Image"/></a></div>
          </li>
          <li class="col-md-3 col-sm-6 col-xs-6">
            <div class="row"><a href="#!"><img src="{{ URL::to('uploaded/images/img13.jpg') }}" alt="Widget Image"/></a></div>
          </li>
        </ul>
      </div>
      <div class="container">
        {{-- <div class="col-md-12 padding-top text-center"><a class="btn btn-link">FOLLOW US </a></div> --}}
      </div>
    </section>
    <footer>
      <div class="container">
        <div data-os-animation="fadeInUp" data-os-animation-delay="0s" class="col-md-12 text-center os-animation">
          <div class="section-head"> 
            <h4>NEWSLETTER</h4>
            <p>Subscribe to our newsletter to receive news, updates	and the latest projects we are working on.</p>
          </div>
        </div>
        <div data-os-animation="fadeInUp" data-os-animation-delay="0s" class="col-md-12 margin-top margin-bottom os-animation">
          <form class="newsletter-form text-center">
            <input placeholder="YOUR EMAIL ADDRESS..." class="form-control"/>
            <button type="submit">I'M IN</button>
            <div class="col-md-12 text-center margin-top"><small>You have our word, no spam. Ever.</small></div>
          </form>
        </div>
        <div class="col-md-12 margin-top">
          <hr/>
        </div>
        <div class="col-md-6 text-left small-screen-text-center">
          <div class="footer-message">
            <p>2017</p>
          </div>
        </div>
        <div class="col-md-6 text-right small-screen-text-center">
          <ul class="social-icons">
            <li><a href="#">Facebook</a></li>
            <li><a href="#">TWITTER</a></li>
          </ul>
        </div>
      </div>
    </footer>

@endsection