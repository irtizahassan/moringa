@extends('frontend.layouts.master')
@section('content')

	<div class="row">
		<div class=" col s12 m8 l9 pagecontent" style="padding: 20px; text-align: justify;">
			
			{!! $page->content !!}

		</div>

		<div class="col s3 m4 l3 hide-on-small-only" style="height: 100%">
			@component('frontend.components.newsfeed')
				@slot('newsfeed',$newsfeed)
	        @endcomponent
	        {{-- <div style="margin:20px 0; border:1px black dashed; padding:8px 10px;">
				<h3 style="text-align: center; font-size: .9rem; margin: 0;">Subscribe To Newsletter</h3>
				<input type="text" style="width: 100%;
										    margin: 0;
										    border: 1px solid black;
										    border-radius: 10px;
										    font-size: 10px;
										    height: auto;
										    padding: 10px 0 !important;
										    margin-top: 5px;
										    text-align: center;"
										    placeholder="Enter Email Address Here"
										    required="">
				<input type="submit" 
						class="btn" 
						style="width: 100%;
							    border-radius: 10px;
							    margin: 5px 0;
							    background-color: #23d160;
							    font-size: .8rem;
							    height: auto;"
						value="Subscribe">
			</div> --}}
		</div>
		
	</div>

@endsection

@section('style')

	<style>
		.pagecontent img{
			max-width: 100% !important;
			height: auto;
		}
	</style>

@endsection