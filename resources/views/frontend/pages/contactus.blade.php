@extends('frontend.layouts.master')
@section('content')


	<h3 style="font-family: initial; text-align: center;">Contact Us</h3>
	
	@if(Session::has('status'))
		<h3 style="font-family: initial; text-align: center; background-color: green; color: white">Message Received</h3>
	@endif

	<div class="row">

		<div class="col m6 s12" style="display: flex; justify-content: center; align-items: center; min-height: 200px">
			<div style="margin-top: 17px">
                @if($contactInfo['phone'] != "")
                	Contact Number
                    <span class="" style="font-size: 0.9rem; text-align: left; display: block;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-phone"></i> 
                         {{$contactInfo['phone']}}  
                    </span>
                @endif
                @if($contactInfo['email'] != "")
                	Email:
                    <span class="" style="font-size: 0.9rem; text-align: left; display: block;">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-envelope"></i> 
                         <a href="mailto:{{$contactInfo['email']}}" style="color:black">{{$contactInfo['email']}}</a>  
                    </span>
                @endif
            </div>
		</div>
	    <form class="col m6 s12" method="post" action="{{url('contactus/process')}}">
			
			{{csrf_field()}}
		    <div class="row">
		        <div class="input-field col s6">
		          <input placeholder="john" id="first_name" type="text" name="first_name" class="validate" required="">
		          <label for="first_name">First Name*</label>
		        </div>
		        <div class="input-field col s6">
		          <input placeholder="doe" id="last_name" type="text" name="last_name" class="validate">
		          <label for="last_name">Last Name</label>
		        </div>
		    </div>
		    <div class="row">
		        <div class="input-field col s12">
		          <input placeholder="Enter your email address here" id="email" type="email" name="email" class="validate" required="">
		          <label for="email">Email*</label>
		        </div>
		    </div>
		    <div class="row">
		        <div class="input-field col s12">
		          <input placeholder="Enter your email address here" id="number" type="text" name="phone" class="validate" required="" pattern="\d{11,13}" value="92">
		          <label for="email">Phone No*</label>
		        </div>
		    </div>
		    <div class="input-field col s12">
		          <textarea placeholder="Type your message here" id="textarea1" class="materialize-textarea" name="message" required=""></textarea>
		          <label for="textarea1">Message*</label>
		    </div>
		    <button class="btn waves-effect waves-light right" type="submit" name="action" >Submit
			    <i class="fa fa-paper-plane" aria-hidden="true"></i>
			</button>

	    </form>
	</div>

@endsection