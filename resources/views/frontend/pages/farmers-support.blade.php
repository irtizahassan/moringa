@extends('frontend.layouts.master')
@section('content')


	<div class="row">

		<h3 style="font-family: initial; text-align: center;">Farmer's Support</h3>
		@if(Session::has('status'))
			<h3 style="font-family: initial; text-align: center; background-color: green; color: white">Messsage Received</h3>
		@endif

	    <form class="col m6 s12 offset-m3" method="post" action="{{url('farmersupport/process')}}">

			{{csrf_field()}}
		    <div class="row">
		        <div class="input-field col s6">
		          <input placeholder="john" id="first_name" type="text" class="validate" name="first_name" required="">
		          <label for="first_name">First Name</label>
		        </div>
		        <div class="input-field col s6">
		          <input placeholder="doe" id="last_name" type="text" class="validate" name="last_name">
		          <label for="last_name">Last Name</label>
		        </div>
		    </div>
		    <div class="row">
		        <div class="input-field col s12">
		          <input placeholder="Enter your email address here" id="email" type="email" name="email" class="validate" required="">
		          <label for="email">Email*</label>
		        </div>
		    </div>
		    <div class="row">
		        <div class="input-field col s12">
		          <input placeholder="Enter Your Phone Number Here" id="number" type="text" class="validate" name="phone" required="" pattern="\d{11,13}" value="92">
		          <label for="number">Phone No</label>
		        </div>
		    </div>
		    <div class="input-field col s12">
		          <textarea placeholder="Type your message here" id="textarea1" class="materialize-textarea" name="message" required=""></textarea>
		          <label for="textarea1">Message*</label>
		    </div>
		    <button class="btn waves-effect waves-light right" type="submit" name="action" >Submit
			    <i class="fa fa-paper-plane" aria-hidden="true"></i>
			</button>

	    </form>
	</div>

@endsection