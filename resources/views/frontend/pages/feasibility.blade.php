@extends('frontend.layouts.master')
@section('content')


    <div class="row">
        <h3>Click below to download Moringa feasibility study. </h3>
    </div>
    <div class="row center">
        <a href="{{URL::to('feasibility.pdf')}}" class="btn btn-large btn-success"><i class="fa fa-download"></i> Download</a>
    </div>

@endsection