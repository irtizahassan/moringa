@extends('frontend.layouts.master')
@section('content')


    <style>
    body {
        background-image: url("https://crystalseye.files.wordpress.com/2011/08/dsc_0724.jpg");
        background-size: cover;
        background-repeat: no-repeat;
    }

    .jumbotron {
        background: #222;
        width: 60%;
        margin: auto;
        margin-top: 30px;
        padding-top: 10px;
    }

    .jumbotron .title {
        font-family: "Georgia";
        color: #FFFFFF;
        padding-top: 40px;
        padding-bottom: 20px;
    }

    .jumbotron .weather {
        color: #FFFFFF;
    }

    p.temp-type {
        padding-top: 10px;
        cursor: pointer;
        color: #FFFFFF;
    }

    h2 {
        font-size: 30px;
    }

    @media screen and (max-width: 700px) {
        h2 {
            font-size: 20px;
        }
    }

    h3 {
        color: #FFFFFF;
        margin: 6px;
        font-size: 20px
    }

    @media screen and (max-width:700px) {
        h3 {
            font-size: 15px;
        }
    }
</style>
<div>

    <div class="container" id="body">
        <div class="jumbotron">
            <div class="row">
                <h3 class="center title"> Current Weather</h3>
            </div>
            <div class="row">
                <h2 id="weather-location" class="center weather">---</h2>
            </div>
            <div class="row">
                <h2 id="weather-current" class="center weather">--</h2>
            </div>
            <div class="row">
                <h3 id="weather-high" class="center weather">High: --</h3>
            </div>
            <div class="row">
                <h3 id="weather-low" class="center weather">Low: --</h3>
                <div class="row">
                    <p id=temp-type class="center temp-type">Fahrenheit</p></div>
            </div>
        </div>
        <div></div>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        getLocation();
    })

    function getLocation() {
        var location;
        $.ajax({
            format: "jsonp",
            dataType: "jsonp",
            url: "http://ip-api.com/json",
            success: function(data) {
                // location = (data.lat + "," + data.lon);
                // $("#weather-location").html(data.city + ", " + data.region);
                // getURL(location)
                httpsLocation();
            },
            error: function() {
                httpsLocation();
            },
            method: "GET"
        });

        function httpsLocation() {
            if (navigator.geolocation) {
                var location;
                navigator.geolocation.getCurrentPosition(passLocation);
            }
        }

        function passLocation(position) {
            location = position.coords.latitude + ", " + position.coords.longitude;
            setCity(location);
            getURL(location);
        }
    }

    function setCity(latLon) {
        var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latLon + "&sensor=true";
        url = url.replace(/\s/g, "");
        $.ajax({
            format: "json",
            dataType: "json",
            url: url,
            success: function(data) {
                $('#weather-location').html(data.results[0].address_components[2].long_name);
            },
            method: "GET"
        });
    }

    function getURL(location, tempSetting) {
        var url = ("https://api.darksky.net/forecast/912496cc41dbd8329b00a4012545e368/" + location);
        //console.log(url);
        getJson(url);

    }

    function getJson(url) {
        //console.log("started getJson with this url: " + url);

        $.ajax({
            format: "jsonp",
            dataType: "jsonp",
            url: url,
            success: function(json) {
                //console.log("great success");
                $("#weather-current").html(Math.round(json.currently.temperature) + "°");
                $("#weather-high").html("High: " + Math.round(json.daily.data[0].temperatureMax) + "°");
                $("#weather-low").html("Low: " + Math.round(json.daily.data[0].temperatureMin) + "°");
                setBackground(json.currently.icon);
            }

        })
            .error(function(jqXHR, textStatus, errorThrown) {
                alert("error: " + JSON.stringify(jqXHR));
            })
    }

    $("#temp-type").on("click", function() {
        var currentTemp = $("#weather-current").html().replace(/°/g, "");
        var highTemp = $("#weather-high").html().replace(/°/g, "");
        var lowTemp = $("#weather-low").html().replace(/°/g, "");
        lowTemp = lowTemp.replace("Low: ", "");
        highTemp = highTemp.replace("High: ", "");

        if ($("#temp-type").html() == "Fahrenheit") {
            $("#weather-current").html(toCelsius(currentTemp) + "°");
            $("#weather-high").html("High: " + toCelsius(highTemp) + "°");
            $("#weather-low").html("Low: " + toCelsius(lowTemp) + "°");
            $("#temp-type").html("Celsius");

        } else if ($("#temp-type").html() == "Celsius") {
            $("#weather-current").html(toFahrenheit(currentTemp) + "°");
            $("#weather-high").html("High: " + toFahrenheit(highTemp) + "°");
            $("#weather-low").html("Low: " + toFahrenheit(lowTemp) + "°");
            $("#temp-type").html("Fahrenheit");

        }

        function toCelsius(num) {
            var newNum = (parseInt(num) - 32) * 5 / 9;
            return Math.round(newNum);
        }

        function toFahrenheit(num) {
            var newNum = (parseInt(num) * 9 / 5) + 32;
            return Math.round(newNum);
        }

    })

    function setBackground(weatherIcon) {
        //console.log(weatherIcon);
        switch (weatherIcon) {
            case "clear-day":
                document.getElementById("body").style.backgroundImage = 'url("http://feelgrafix.com/data_images/out/15/899301-sunny-day.jpg")';
                break;
            case "clear-night":
                document.getElementById("body").style.backgroundImage = 'url("https://tcklusman.files.wordpress.com/2014/05/tumblr_static_dark-starry-night-sky-226736.jpg")';
                break;
            case "rain":
                document.getElementById("body").style.backgroundImage = 'url("http://wearechange.org/wp-content/uploads/2015/03/1_See_It.jpg")';
                break;
            case "cloudy":
                document.getElementById("body").style.backgroundImage = 'url("http://www.tripwire.com/state-of-security/wp-content/uploads/cache//shutterstock_106367810/4261234929.jpg")';
                break;
            case "partly-cloudy-day":
                document.getElementById("body").style.backgroundImage = 'url("http://www.sturdyforcommonthings.com/wp-content/uploads/2013/03/wind_blowing.jpg")';
                break;
            case "partly-cloudy-night":
                document.getElementById("body").style.backgroundImage = 'url("http://scienceblogs.com/startswithabang/files/2013/04/night-sky-stars.jpeg")';
                break;
            case "snow":
                document.getElementById("body").style.backgroundImage = 'url("http://www.vancitybuzz.com/wp-content/uploads/2015/12/shutterstock_315123593-984x500.jpg")';
                break;
            default:
                break;

        }

    }
</script>

@endsection