<!DOCTYPE html>
<html>
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="google-site-verification" content="jtIRmhZEP17H0mSSc1xheF9a6_LRuJtQrj_XxKskwZo" />

    @if(isset($title))
        <title>{{ $title }}</title>
    @else
        <title>MoringForLife.pk</title>
    @endif

    <link type="text/css" rel="stylesheet" href="{{URL::to('materialize/css/materialize.css')}}"  media="screen,projection"/>
    <link rel="stylesheet" href="{{ URL::to('font-awesome/css/font-awesome.css') }}" type="text/css" media="all"/>

    <style>
            @import url(https://fonts.googleapis.com/css?family=Roboto:300);

            section {
                position: relative;
                width: 400px;
                background-color: #fff;
                box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.3);
                overflow: hidden;
                margin: 20px auto;
                border-radius: 2px;
            }

            .header {
                width: 100%;
                background-color: #23d160;
                overflow: hidden;
                padding-bottom: 24px;
            }
            .header .circle {
                position: relative;
                top: 20px;
                left: 0px;
                width: 50px;
                height: 50px;
                background-color: #23d160;
                box-shadow: 0px 3px 3px rgba(0, 0, 0, 0.3);
                border-radius: 50%;
                z-index: 10;
            }
            .header h3 {
                position: relative;
                top: 15px;
                left: 15px;
                font-size: 1.2rem;
                color: #fff;
                border-bottom: 1px solid white;
            }

            .line {
                position: absolute;
                z-index: 0;
                top: 40px;
                left: 25px;
                height: calc(100% + 200px);
                width: 0px;
                border-right: 1px solid #23d160;
            }

            #content {
                width: 100%;
                height: 237px;
                overflow: hidden;
                padding-top: 0px;
                overflow-y: auto;
                overflow-x: hidden;
            }
            #content::-webkit-scrollbar {
                width: 6px;
                background-color: #EBEBEB;
            }
            #content::-webkit-scrollbar-thumb {
                background-color: #ccc;
            }
            #content .object {
                width: 100%;
            }
            #content .object:after {
                content: "";
                display: block;
                height: 0;
                clear: both;
            }
            #content .object .date {
                float: left;
                width: 40px;
                height: 40px;
                margin: 10px;
                background-color: #76ff03;
                color: #fff;
                border-radius: 10px;
                text-align: center;
                line-height: 1.9em;
                font-size: 1.4em;
            }
            #content .object .circle {
                position: relative;
                float: left;
                z-index: 10;
                left: 4px;
                margin: 15px 0 0 15px;
                width: 12px;
                height: 12px;
                background-color: #23d160;
                border-radius: 50%;
            }
            #content .object .context {
                float: left;
                color: #666;
                width: 90%;
                min-height: 40px;
                margin: -15px 10px 0px 25px;
                padding: 0 17px;
                line-height: 1.5em;
                text-align: justify;
                font-size: .8rem;
                word-wrap: break-word;
            }
            .indicators{
                /*display: none;*/
            }
            .card-header-title {
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                color: #363636;
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-flex: 1;
                -ms-flex-positive: 1;
                flex-grow: 1;
                font-weight: 700;
                padding: 0.75rem;
            }
            p{
                margin: 0
            }
            .card-header {
                -webkit-box-align: stretch;
                -ms-flex-align: stretch;
                align-items: stretch;
                -webkit-box-shadow: 0 1px 2px rgba(10, 10, 10, 0.1);
                box-shadow: 0 1px 2px rgba(10, 10, 10, 0.1);
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
            }
            .card-content{
                text-align: justify;
                font-size: 0.8rem;
            }
            body{
                background: url({{URL::to('images/bgimg.jpg')}}) no-repeat center center fixed;
                background-size: cover;
            }
            .social-media{
                display: flex;
                padding: 5px;
                background-color: white;
            }

            .social-media img{
                margin: 5px;
            }

            nav .ul {
                margin: 0;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                justify-content: center;
            }

            nav .ul li {
                -webkit-transition: background-color .3s;
                transition: background-color .3s;
                float: left;
                padding: 0;
                line-height: 35px;
                border-bottom: 1px rgba(0,0,0,0.3) solid;
            }

            nav ul a{
                font-size: 1rem !important;
            }

            .marquee {
                width: 100%;
                border: 1px solid rgba(0,0,0,0.5);
                color: black;
                white-space: nowrap;
                overflow: hidden;
                box-sizing: border-box;
            }
            .marquee p {
                display: inline-block;
                padding-left: 100%;
                margin: 5px;
                animation: marquee 15s linear infinite;
            }

            .marquee .title{

                position: absolute;
                padding: 5px;
                margin-left: -11px;
                background: white;
                z-index: 2;
                border-right: 1px solid black;
                font-variant-caps: all-small-caps;

            }

            .unselectable {
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .marquee p:hover{
                -webkit-animation-play-state: paused;
                -moz-animation-play-state: paused;
                -o-animation-play-state: paused;
                 animation-play-state: paused;
            }
            .abcde{
                background-image: url({{URL::to('images/moringa-header.png')}});
                height: 90px;
                padding: 0 !important;
            }
            .abcde a{
                font-size: 0.80rem !important;

            }
            .logoimg{
                height: 90px;
                background-image: url({{URL::to('images/logo-bg.jpg')}});
            }
            .yo{
                border: 2px solid #eee;
            }
            .dropdowna{
                line-height: 20px;
                height: auto;
                min-height: auto !important;
                padding:0 10px;
            }
            .dropdownb{
                line-height: auto;
                height: auto;
                padding: 5px 0 !important;
                text-align: center;
            }
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 15% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                display: inline-block;
            }

            /* The Close Button */
            .close {
                color: #aaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }
            @keyframes marquee {
                0%   { transform: translate(0, 0); }
                100% { transform: translate(-100%, 0); }
            }
            @media only screen and (max-width: 598px)  {
                .abcde{
                    background: transparent;
                    height: 130px;
                    padding: 0 !important;
                }
                .yo{
                    width: 100%;
                    margin: 3px 0;
                }
                .logoimg{
                    height: 130px;
                }
            }
            @media only screen and (max-width: 645px)  {

            }
        </style>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126534473-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-126534473-1');
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126534473-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-126534473-2');
        </script>



        @yield('style')

    </head>
    <body>



        <div style="max-width: 1000px; margin: auto; background-color: white;">

            @component('frontend.components.bulletin')
                @slot('bulletin',$bulletin)
            @endcomponent

            <div class="row" style="margin-bottom: 0px;">
                <div class="col m4 s7 logoimg" style="display: flex; align-items: center;">
                    {{-- <h6 style="margin: 0; text-align: right; font-size: 1.1rem; padding-top: 5px; color: #4a4a4a">
                        @if($contactInfo['phone'] != "")
                            <span class="" style="font-size: 0.9rem">
                                <i class="fa fa-phone"></i>
                                 {{$contactInfo['phone']}}
                            </span>
                            @endif
                            @if($contactInfo['email'] != "")
                            <span class="" style="font-size: 0.9rem">
                                <i class="fa fa-envelope"></i>
                                 {{$contactInfo['email']}}
                            </span>
                        @endif
                    </h6> --}}
                    <div class="center-align unselectable" style="padding: 10px">
                        <a href="{{url('/')}}">
                            <img src="{{URL::to('images/logo.png')}}" width="100%" alt="">
                        </a>
                    </div>
                </div>
                <div class="col s5 m8 right-align abcde" style="padding: 10px">
                    <div class="row" style="text-align: center; padding: 4px; background-color: #eee; margin: 0 !important;">
                
                        <div class="col m3 z-depth-2 waves-effect yo" style="padding:0 3px; background-color: #63ab38;">
                           <a href="{{url('page/Introduction')}}" style="color:white; white-space:nowrap; " class="">
                                Introduction
                            </a>
                        </div>

                        <div class="col m3 z-depth-2 waves-effect yo dropdown-button"  data-activates='dropdown1' style="padding:0 3px; background-color: #01573e;">
                             <a style="color:white; white-space:nowrap;" href="{{url('page/Moringa_Nursery')}}">
                                 Moringa Nursery
                             </a>
                        </div>

                        <div class="col m3 z-depth-2 waves-effect yo" style="padding:0 3px; background-color: #eaa003;">
                            <a href="{{url('page/Moringa_Products')}}" style="color:white; white-space:nowrap;">
                                Moringa Products
                            </a>
                        </div>
                        <div class="col m3 z-depth-2 waves-effect yo" style="padding:0 3px; background-color: #578324;">
                        <a  href="{{url('Moringa_Seeds')}}" style="color:white; white-space:nowrap;">
                           Moringa Seeds
                        </a>
                        </div>


                        <ul id='dropdown2' class='dropdown-content'>
                            <li class="dropdowna">
                                <a href="{{url('page/moringa_cultivation_urdu')}}" class="dropdownb">
                                    Urdu
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Cultivation_English')}}">
                                    English
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Cultivation_Chart')}}">
                                    Cultivation Chart
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

                <div class="row show-on-small hide-on-med-and-up" style="margin-bottom: 0">
                    <div class="col s12 " style="overflow: hidden">
                        <img src="{{URL::to('images/moringa-header.png')}}" alt="" style="width: 100%; height: 100%;">
                    </div>
                </div>
            <nav>
                <div class="nav-wrapper light-green accent-4" style="background-color: #23d160 !important;">
                    <a href="{{url('/')}}" class="brand-logo" style="font-family: cursive; padding-left: 10px; font-size: 1.5rem;"></a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
                    <ul class="right hide-on-med-and-down ul" style="background-color:#23d160;">
                        <li><a href="{{url('/')}}">Home</a></li>
                        <li class="col m3 dropdown-button"  data-activates='dropdown5' style="">
                             <a style="color:white; white-space:nowrap;">
                                 Moringa Oleifera
                             </a>
                        </li>

                        <ul id='dropdown5' class='dropdown-content'>
                       
                     {{--       <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Health_Benefits')}}">
                                    Health Benefits
                                </a>
                            </li>--}}
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('Moringa_Feasibility')}}">
                                    Feasibility
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Certification')}}">
                                    Certification
                                </a>
                            </li>

                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Experts')}}">
                                    Experts
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('Moringa_Gallery')}}">
                                    Gallery
                                </a>
                            </li>
                        </ul>
                        <li class="col m3 dropdown-button"  data-activates='dropdown2' style="">
                            <a style="color:white; white-space:nowrap;">
                                Moringa Cultivation
                            </a>
                        </li>

                        <li class="col m3 dropdown-button"  data-activates='dropdown6' style="">
                           <a style="color:white; white-space:nowrap;">
                                 Other Local Trees
                             </a>
                        </li>

                        <ul id='dropdown6' class='dropdown-content'>
                            <li class="dropdowna">
                                <a href="{{url('page/Kikar')}}" class="dropdownb">
                                    Kikar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a href="{{url('page/Kachnar')}}" class="dropdownb">
                                    Kachnar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Amaltas')}}">
                                    Amaltas
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Gul_Mohar')}}">
                                    Gul_Mohar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Pipal')}}">
                                    Pipal
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Bakain')}}">
                                    Bakain
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Ber')}}">
                                    Ber
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Gul-i-Nishter')}}">
                                    Gul-i-Nishter
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Jamun')}}">
                                    Jamun
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Australian_Kikar')}}">
                                    Australian_Kikar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Neem')}}">
                                    Neem
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Simal')}}">
                                    Simal
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Shisham')}}">
                                    Shisham
                                </a>
                            </li>
                        </ul>
                        @foreach($menuItems as $item)
                            <li><a href="{{url($item['url'])}}">{{$item['name']}}</a></li>
                        @endforeach
                        <li class="col m3 dropdown-button"  data-activates='dropdown6' style="background-color: #23d160;">
                            <a style="color:white; white-space:nowrap;">
                                Other Local Trees
                            </a>
                        </li>
                        <li><a href="{{url('/contactus')}}">Contact Us</a></li>
                    </ul>
                    <ul class="side-nav" id="mobile-demo">
                        <li style="background-color: #23d160; border:1px white solid"><a href="{{url('/')}}">Home</a></li>
                        <li class="col m3 dropdown-button"  data-activates='dropdown7' style="background-color: #23d160; border:1px white solid">
                             <a style="color:white; white-space:nowrap;">
                                 Moringa Oleifera
                             </a>
                        </li>

                        <ul id='dropdown7' class='dropdown-content'>
                          {{--  <li class="dropdowna">
                                <a href="{{url('page/Introduction')}}" class="dropdownb">
                                    Introduction
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a href="{{url('page/Moringa_Products')}}" class="dropdownb">
                                    Products
                                </a>
                            </li>--}}
                            {{--<li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Health_Benefits')}}">
                                    Health Benefits
                                </a>
                            </li>--}}
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('Moringa_Feasibility')}}">
                                    Feasibility
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Moringa_Certification')}}">
                                    Certification
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('Moringa_Gallery')}}">
                                    Gallery
                                </a>
                            </li>
                        </ul>

                        <li class="col m3 dropdown-button"  data-activates='dropdown2' style="background-color: #23d160; border:1px white solid">
                            <a style="color:white; white-space:nowrap;">
                                Moringa Cultivation
                            </a>
                        </li>

                        <ul id='dropdown8' class='dropdown-content'>
                            <li class="dropdowna">
                                <a href="{{url('page/Kikar')}}" class="dropdownb">
                                    Kikar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a href="{{url('page/Kachnar')}}" class="dropdownb">
                                    Kachnar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Amaltas')}}">
                                    Amaltas
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Gul_Mohar')}}">
                                    Gul_Mohar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Pipal')}}">
                                    Pipal
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Bakain')}}">
                                    Bakain
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Ber')}}">
                                    Ber
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Gul-i-Nishter')}}">
                                    Gul-i-Nishter
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Jamun')}}">
                                    Jamun
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Australian_Kikar')}}">
                                    Australian_Kikar
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Neem')}}">
                                    Neem
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Simal')}}">
                                    Simal
                                </a>
                            </li>
                            <li class="dropdowna">
                                <a class="dropdownb" href="{{url('page/Shisham')}}">
                                    Shisham
                                </a>
                            </li>
                        </ul>

                        @foreach($menuItems as $item)
                            <li style="background-color: #23d160; border:1px white solid">
                                <a href="{{url($item['url'])}}">{{$item['name']}}</a>
                            </li>
                        @endforeach
                        <li class="col m3 dropdown-button"  data-activates='dropdown8' style="background-color: #23d160; border:1px white solid">
                            <a style="color:white; white-space:nowrap;">
                                Other Local Trees
                            </a>
                        </li>
                        <li style="background-color: #23d160; border:1px white solid"><a href="{{url('/contactus')}}">Contact Us</a></li>

                    </ul>

                </div>
            </nav>

                @yield('content')

            <footer class="page-footer light-green accent-4" style="margin: 0; background-color: #23d160 !important">
                <div class="">
                    <div class="row">
                        <div class="col m6 s12" style="display: flex; align-items: center; justify-content: center; flex-direction: column;">
                            <div style="font-size: 1.1rem; text-align: center;">
                                <span style="text-align: center; padding-top: 10px;">Find Us On</span>
                            </div>
                            <div class="social-media">
                                <img src="{{ URL::to('images/social-media/instagram.svg') }}" alt="" height="25px" width="25px">
                                <img src="{{ URL::to('images/social-media/facebook.svg') }}" alt="" height="25px" width="25px">
                                <img src="{{ URL::to('images/social-media/youtube.svg') }}" alt="" height="25px" width="25px">
                                <img src="{{ URL::to('images/social-media/twitter.svg') }}" alt="" height="25px" width="25px">
                                <img src="{{ URL::to('images/social-media/whatsapp.svg') }}" alt="" height="25px" width="25px">
                            </div>
                            <div style="margin-top: 17px">
                                @if($contactInfo['phone'] != "")
                                    <span class="" style="font-size: 0.9rem; text-align: left; display: block;">
                                        <i class="fa fa-phone"></i>
                                         {{$contactInfo['phone']}}
                                    </span>
                                @endif
                                @if($contactInfo['email'] != "")
                                    <span class="" style="font-size: 0.9rem; text-align: left; display: block;">
                                        <i class="fa fa-envelope"></i>
                                         <a href="mailto:{{$contactInfo['email']}}" style="color:white">{{$contactInfo['email']}}</a>
                                    </span>
                                @endif
                                @if(isset($contactInfo['email2']) && $contactInfo['email2'] != "")
                                    <span class="" style="font-size: 0.9rem; text-align: left; display: block;">
                                        <i class="fa fa-envelope"></i>
                                         <a href="mailto:{{$contactInfo['email2']}}" style="color:white">{{$contactInfo['email2']}}</a>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col m2"></div>
                        {{-- <div class="col m4 hide-on-small-only">
                            <h5 class="white-text" style="font-size: 1.1rem; margin: 0">Site Map</h5>
                            <ul>
                                <li><a class="grey-text text-lighten-3" href="#!" style="font-size: .9rem">Link 1</a></li>
                                <li><a class="grey-text text-lighten-3" href="#!" style="font-size: .9rem">Link 2</a></li>
                                <li><a class="grey-text text-lighten-3" href="#!" style="font-size: .9rem">Link 3</a></li>
                                <li><a class="grey-text text-lighten-3" href="#!" style="font-size: .9rem">Link 4</a></li>
                            </ul>
                        </div> --}}
                    </div>
                </div>
                <div class="footer-copyright center-align" style="background-color: white; color: #4a4a4a">
                        <p style="width: 100%; text-align: center;">© 2018 - 2019 moringaforlife.pk . All rights reserved. Powered By: Compresol</p>
                </div>
            </footer>
        </div>
        <div id="popupModal" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <img src="{{asset('images/popup.jpg')}}" alt="">
          </div>

        </div>
        <script type="text/javascript" src="{{ URL::to('js/jquery-3.2.1.min.js') }}"></script>
        <script type="text/javascript" src="{{URL::to('materialize/js/materialize.min.js')}}"></script>
        <script>
            $(document).ready(function(){
                @if(session("popupShownAlready","false") == "false" && 1 == 2)
                    //setTimeout(function() {
                    //    $("#popupModal").css("display","flex");
                    //}, 5000);
                    {{session(["popupShownAlready" => "true"])}}
                @endif
            });

            var modal = document.getElementById('popupModal');
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }

            $(".button-collapse").sideNav();

            $('.dropdown-button').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrainWidth: true, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: true, // Displays dropdown below the button
                    alignment: 'left', // Displays dropdown with edge aligned to the left of button
                    stopPropagation: true // Stops event propagation
                }
            );

            $('.moringa_cultivation').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrainWidth: true, // Does not change width of dropdown to that of the activator
                    hover: true, // Activate on hover
                    gutter: 0, // Spacing from edge
                    belowOrigin: true, // Displays dropdown below the button
                    alignment: 'left', // Displays dropdown with edge aligned to the left of button
                    stopPropagation: true // Stops event propagation
                }
            );
        </script>

        @yield('script')

    </body>
</body>
</html>
