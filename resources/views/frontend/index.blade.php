@extends('frontend.layouts.master')
@section('content')

	<div class="row" style="margin-bottom: 0px">
        <div class="col l9 m8 s12" style="margin-top: 20px;">

			@component('frontend.components.slider')
            @endcomponent

        </div>
        <div class="col l3 s12 m4">

            @component('frontend.components.newsfeed')
				@slot('newsfeed',$newsfeed)
            @endcomponent

        </div>
    </div>
    <div class="row">
        <div class="col l6 m6 s12">

			@component('frontend.components.card')

                @slot('heading','About Moringa')
                @slot('icon','fa-info-circle')

                Moringa oleifera Lam is native to the India and Pakistan
                and has become domesticaed in the tropical and
                subtropical areas around the world.
                All parts of the Moringa tree are being
                used as food, herbal food supplements,
                cosmetics and medicine.
                Moringa oleifera is extremely
                rich in crucial vitamins, minerals
                and amino-acids.

                That makes moringa leaf an
                ideal natural source for almost all
                the vitamins, minerals that energise
                our body craves.  <a href="{{url('page/Introduction')}}" target="_blank">Readmore</a>

            @endcomponent

        </div>
        <div class="col l6 m6 s12">

            @component('frontend.components.card')

                @slot('heading',$business['heading'])
                @slot('icon','fa-pagelines')

                {!! $business['details'] !!}

            @endcomponent

        </div>
    </div>
    <div class="row" style="padding: 0 0.75rem">
        <div class="col m6 s12" style="background-image: url({{URL::to('uploaded/images/side.jpg')}}); height: 300px; background-size: cover; background-position: center;">
        </div>
        <div class="col m6 s12 z-depth-4" style="min-height: 300px; color: rgba(74,74,74)">
            <div style="padding: 10px">
                <h4 style="font-size: 1.5rem"><a href="#" style="color: black">Moringa Benefits</a></h4>
                <p style="font-size: 0.8rem; text-align: justify;">Moringa oleifera is a plant that has been praised for its health benefits for thousands of years. It is very rich in healthy antioxidants and bioactive plant compounds. So far, scientists have only investigated a fraction of the many reputed health benefits. Moringa is known around the world as a rich source of vegetable protein (30%). It contains 18amino acids including all 8 essential amino acids, 46 active antioxidants, 36 anti-inflammatories. Moringa oleifera is nature’s whole-food complex, which makes it easy for your body to recognize, absorb and assimilate. Recent studies prove it to be the most potent super food in existence. Moringa leaf powder is also rich in flavonoids, such as catching polyphenols and especially epigallocatechin gallate (EGCG). It has been suggested that these extremely powerful antioxidants may have therapeutic applications in the treatment of many medical disorders and overall well-being</p>
                <ol style="font-size: 0.8rem">
                    <li>Moringa oleifera Is Very Nutritious</li>
                    <li>It Is Rich in Antioxidants</li>
                    <li>Protect Against Arsenic Toxicity</li>
                    <li>Helps reduce some diabetes symptoms</li>
                    <li>Protects the cardiovascular system</li>
                </ol>
                <p style="font-size: 0.8rem; text-align: justify;">
                    Benefits don’t end here there are many more
                    <a href="{{url('/page/Moringa_Health_Benefits')}}">Readmore</a>
                </p>
            </div>
        </div>

        <div class="col m6 s12 z-depth-4" style="min-height: 300px; color: rgba(74,74,74)">
            <div style="padding: 10px">
                <h4 style="font-size: 1.5rem"><a href="#" style="color: black">Moringa Products</a></h4>
                <p style="font-size: 0.8rem; text-align: justify;">The Moringa Tree could virtually wipe out malnutrition. The countries with the highest rates of malnutrition are almost the same where Moringa trees grows best. Given the world food crisis, the use of local resources like Moringa Trees is critical to improve nutrition among poor households. Some products made from it are</p>
                <ol style="font-size: 0.8rem">
                    <li>Moringa Leaves</li>
                    <li>Moringa Powder</li>
                    <li>Biostimulant</li>
                    <li>Moringa Oil</li>
                    <li>Moringa Tea</li>
                    <li>Moringa Capsules</li>
                    <li>Moringa Tablets</li>
                </ol>
                <p style="font-size: 0.8rem; text-align: justify;">This is just the beginning of the list there is much more and to know further details about these products <a href="{{url('/page/Moringa_Products')}}">Readmore</a></p>
            </div>
        </div>
        <div class="col m6 s12" style="background-image: url({{URL::to('uploaded/images/side-right.jpg')}}); height: 300px; background-size: cover; background-position: center;">
        </div>
    </div>

    <div class="row" style="margin: 0">
        <div class="center-align">
            <div class="col s4"></div>
            <div class="col s4 center-align">
                <h3 style="margin: 0; font-size: 1.7rem">Videos</h3>
                <hr >
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 l4 center-align">
            <iframe src="https://www.youtube.com/embed/W8mPP9fynWg?rel=0"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen>
            </iframe>
        </div>
        <div class="col s12 m12 l4 center-align">
            <iframe src="https://www.youtube.com/embed/LEbb-62GRqE?rel=0"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen>
            </iframe>
        </div>
        <div class="col s12 m6 l4 center-align">
            <iframe src="https://www.youtube.com/embed/gwaSLcz2XZc"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen>
            </iframe>
        </div>
        <div class="col s12 m6 l4 center-align">
            <iframe src="https://www.youtube.com/embed/bvydupsBATg"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen>
            </iframe>
        </div>
        <div class="col s12 m6 l4 center-align">
            <iframe src="https://www.youtube.com/embed/h8ZEid71itQ"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen>
            </iframe>
        </div>
        <div class="col s12 m6 l4 center-align">
            <iframe src="https://www.youtube.com/embed/Ey_dGjQU2yw"
                    frameborder="0"
                    gesture="media"
                    allow="encrypted-media"
                    allowfullscreen></iframe>
            </iframe>
        </div>

    </div>
    <div class="row" style="margin: 0">
        <div class="center-align">
            <div class="col l4 m4 s12 center-align">
                @component('frontend.components.card')

                    @slot('heading',$research['heading'])
                    @slot('icon','fa-home')

                    {!! $research['details'] !!}

                @endcomponent
            </div>
            <div class="col l8 m8 s12 center-align">
                @component('frontend.components.card')

                    @slot('heading',"Managing Director")
                    @slot('icon','fa-user')
                <table>
                <tr><td>  <img style="display: block;" src="http://www.moringaforlife.pk/storage/contactus_img.jpg" height="autot" width="200"></div>
            <span style="text-align: justify"> I am Gulrez Shahzad. I have studied M.Sc (Hons.) Agronomy. With the background in agricultural trials and research, and expertise in agribusiness development, I represent G S Agrizone Ltd across the Pakistan and Europe/UK. I oversee the range of projects across the division. Focus areas include agronomy, moringa nursery raising, moringa farming under good agricultural practices (GAP), moringa leaf extract, moringa as livestock feed, moringa biostimulants and product development. I am passionate about working on this remarkable nutritious herbal tree “Moringa oleifera" to explore its benefits to impact the climate change.
            </span>
                </td></tr>
                  </table>


                @endcomponent
            </div>
        </div>
    </div>

    <div class="row" style="margin: 0; padding: 20px; border: 2px dashed">
        <div class="center-align col l6 offset-l3 m8 offset-m2 s12">
            <h4 style="margin: 0; font-family: initial;">Subscribe To Our Newsletter</h4>
            <h6 style="margin: 0; font-family: cursive;">Get Important Updates</h6>
            <form id="newsletter-form">
                {{csrf_field()}}
                <input type="email" name="email" style="border: 1px black solid !important;
                                            margin: 10px 0;
                                            border-radius: 20px;
                                            color: black !important;
                                            text-align: center;"
                                            placeholder="Enter Your Email Address Here"
                                            required="">
                <input type="submit" id="newsletter-submit" class="btn" style="border-radius: 20px; background-color: #23d160;" value="Subscribe">
            </form>
            <h5 style="font-family: initial; font-variant-caps: all-petite-caps; display: none;" id="newsletter-subscribe-success">
                You have successfully subscribed to our newsletter
            </h5>
        </div>
    </div>

@endsection

@section('style')

    <style>

        input, input:focus, textarea, textarea:focus{
            border-color: white !important;
            color: white !important;
        }

        label{
            color: white !important;
        }
        .carousel-item{
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

    </style>

@endsection

@section('script')

    <script>
        $('.carousel.carousel-slider').carousel({fullWidth: true});
        setTimeout(autoplay, 4500);
        function autoplay() {
            $('.carousel').carousel('next');
            setTimeout(autoplay, 4500);
        }

        $('#newsletter-form').on('submit',function(e){
            e.preventDefault();
            $('#newsletter-submit').prop('disabled',true);
            $('#newsletter-submit').val('Processing');

             $.ajax({
                type: "POST",
                url: '{{url(route('newsletter.subscribe'))}}',
                data: $("#newsletter-form").serialize(), // serializes the form's elements.
                success: function(data){
                    if(data.status == "success"){
                        $('#newsletter-subscribe-success').css('display','block');
                        $('#newsletter-form').css('display','none');
                    }else{
                        $('#newsletter-subscribe-success').css('display','block');
                        $('#newsletter-subscribe-success').html('Subscription Failed...');
                        $('#newsletter-form').css('display','none');
                    }
                }
             });
        });

        $(document).ready(function(){

            $('#carousel-abc').css('height','280px');

        });

    </script>

@endsection