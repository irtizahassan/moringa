<section style="width: 100%;">
    <div class="header unselectable" style="background-color: #23d160">
        {{-- <div class="circle"></div> --}}
        <h3 style="margin: 0"> <i class="fa fa-list"></i> &nbsp;Latest News</h3>
    </div>
    <div class="line"></div>
    <div id="content">
    	@if(count($newsfeed) == 0)
    		<div class="object">
	            <div class="circle"></div>
	            <div class="context">
	                No News Yet.
	            </div>
	        </div>
	    @else
	        @foreach($newsfeed as $news)
	            <div class="object">
	                <div class="circle"></div>
	                <div class="context">
	                     {{ $news->content }}
	                </div>
	            </div>
	        @endforeach
	    @endif
    </div>
</section>