@if(count($bulletin) != 0)

	<div class="row" style="margin: 0; padding: 2px; padding-bottom: 0">
	    <div class="col s12 marquee">
	        <div class="title unselectable">Important:</div>
	        <p>
	        	@foreach($bulletin as $item)
					<i class="fa fa-dot-circle-o"></i> 
	        		<a 
	        			@if(trim($item['url']) != "")
							href="{{$item['url']}}" target="_blank" 
	        			@endif
	        		>{{$item['heading']}}</a>
	        		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        	@endforeach
	        </p>
	    </div>
	</div>

@endif