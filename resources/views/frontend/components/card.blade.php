<div class="card">
    @if(isset($heading))
        <header class="card-header" style="background-color: #23d160; padding: 10px">
            <p class="card-header-title unselectable" style="color: white; font-size: 1.2rem; font-weight: 400; border-bottom: 1px solid white; padding: 0; display: inline-table;">
                @if(isset($icon)) 
                    <i class="fa {{$icon}}" aria-hidden="true"></i> 
                @endif 
                {{ $heading }}
            </p>
        </header>
    @endif
    <div class="card-content">
        <div class="content has-text-justified">
            
            {{ $slot }}
        
        </div>
    </div>
</div>