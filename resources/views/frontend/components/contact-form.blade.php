<div class="row" >
    <div class="center-align">
        <div class="col s4"></div>
        <div class="col s4 center-align">
            <h3 style="margin: 0; font-size: 1.7rem">Contact Us</h3>
        </div>
    </div>
</div>
<div class="row">
  <div class="col s2"></div>
  <form class="col s8">
    <div class="row">
      <div class="input-field col s6">
        <input placeholder="John Doe" id="first_name" type="text" class="validate">
        <label for="first_name">First Name</label>
      </div>
      <div class="input-field col s6">
        <input placeholder="email here" id="email" type="email" class="validate">
        <label for="email">Email</label>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea" placeholder="Message Here"></textarea>
          <label for="textarea1">Message</label>
        </div>
      </div>
    </div>
  </form>
</div>