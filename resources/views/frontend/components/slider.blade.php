{{-- <div class="carousel carousel-slider">
    <a class="carousel-item" href="#one!">
        <h2>ABC</h2>
        <p>Yay</p>
        <img src="https://lorempixel.com/580/250/nature/1" style="height: 400px">
    </a>
    <a class="carousel-item" href="#two!">
        <img src="https://lorempixel.com/580/250/nature/2" style="height: 400px">
    </a>
    <a class="carousel-item" href="#three!">
        <img src="https://lorempixel.com/580/250/nature/3" style="height: 400px">
    </a>
    <a class="carousel-item" href="#four!">
        <img src="https://lorempixel.com/580/250/nature/4" style="height: 400px">
    </a>
</div>   --}}  
<div id="carousel-abc" class="carousel carousel-slider center" data-indicators="true" style="height: 280px !important;">
    {{-- <div class="carousel-fixed-item center">
      <a class="btn waves-effect white grey-text darken-text-2" style="background-color: white !important; color: #d2d2d2">Blog</a>
    </div> --}}

    <div class="carousel-item amber white-text"
         style="background-image: url('{{URL::to('uploaded/images/leaf.jpg')}}'); ">
        {{-- <h2>Second Panel</h2>
        <p class="white-text">This is your second panel</p> --}}
    </div>
    <div class="carousel-item amber white-text"
         style="background-image: url('{{URL::to('uploaded/images/powder.jpg')}}'); ">
        {{-- <h2>Second Panel</h2>
        <p class="white-text">This is your second panel</p> --}}
    </div>
    <div class="carousel-item white-text" 
            style="background-image: url('{{URL::to('uploaded/images/img14.jpeg')}}'); ">
      {{-- <div style="background-color: rgba(0,0,0,0.2); height: 100%;">
          <h2 style="margin: 0; padding: 1.78rem 0 1.424rem 0">First Panel</h2>
          <p class="white-text">This is your first panel</p>
      </div> --}}
    </div>
    <div class="carousel-item amber white-text"
            style="background-image: url('https://www.moringaforlife.pk/storage/slider/moringa-flower.jpg'); ">
      {{-- <h2>Second Panel</h2>
      <p class="white-text">This is your second panel</p> --}}
    </div>
    <div class="carousel-item amber white-text"
         style="background-image: url('{{URL::to('uploaded/images/tea_moringa.jpg')}}'); ">
        {{-- <h2>Second Panel</h2>
        <p class="white-text">This is your second panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
            style="background-image: url('{{URL::to('uploaded/images/img1.jpg')}}'); ">
      {{-- <h2>Third Panel</h2>
      <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img2.jpg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img3.jpeg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img5.jpeg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img6.jpeg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img8.jpg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>

    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img9.jpg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>

    <div class="carousel-item green white-text"
         style="background-image: url('{{URL::to('uploaded/images/img10.jpg')}}'); ">
        {{-- <h2>Third Panel</h2>
        <p class="white-text">This is your third panel</p> --}}
    </div>
    <div class="carousel-item blue white-text"
            style="background-image: url('https://www.moringaforlife.pk/storage/slider/moringa-leaves.jpg'); ">
      {{-- <h2>Fourth Panel</h2>
      <p class="white-text">This is your fourth panel</p> --}}
    </div>
  </div>

{{-- <div class="slider">
    <ul class="slides">
        <li>
            <img src="https://lorempixel.com/580/250/nature/1"> <!-- random image -->
            <div class="caption center-align">
                <h3>This is our big Tagline!</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
        <li>
            <img src="https://lorempixel.com/580/250/nature/2"> <!-- random image -->
            <div class="caption left-align">
                <h3>Left Aligned Caption</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
        <li>
            <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
            <div class="caption right-align">
                <h3>Right Aligned Caption</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
        <li>
            <img src="https://lorempixel.com/580/250/nature/4"> <!-- random image -->
            <div class="caption center-align">
                <h3>This is our big Tagline!</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
    </ul>
</div> --}}